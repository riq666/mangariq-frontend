# SimpleDex

SimpleDex is a high contrast readonly frontend for browsing MangaDex v5

**Home** | **Manga**
---  | ---
![](.github/img/HomePage.jpg) | ![](.github/img/MangaPage.jpg)

**Follows** | **Search**
---  | ---
![](.github/img/FollowsPage.jpg) | ![](.github/img/SearchPage.jpg)

## Architecture

This site is a Vue.js Single Page Application (SPA) hosted on Cloudflare Pages. All API requests are proxied through a Cloudflare Worker to bypass CORS restrictions.

All `XHR` requests are cached on the client side through a service worker to minimize network requests.

Route | Cache Duration
---   | ---
Feeds (e.g. `/user/follows/manga/feed`) | 1 hour
Static Assets (`js`, `css`, Google Fonts) | 1 year
Cover Images | 30 days
Specific Resources (e.g. `/manga/{id}`) | 30 days
Other `GET` Routes | 1 week

## Dev

```
yarn dev    # In one console
wranger dev # In another console
```

## Bookmarklet

You can use a [bookmarklet](https://en.wikipedia.org/wiki/Bookmarklet) to redirect your current `mangadex.org` page to the equivalent page on SimpleDex.

Create a new bookmark and use the following URL:

```
javascript:(function()%7B(function%20()%20%7B%0A%20%20%20%20let%20url%20%3D%20new%20URL(document.location.href)%3B%0A%20%20%20%20let%20mangadex%20%3D%20'https%3A%2F%2Fmangadex.org'%0A%20%20%20%20let%20simpledex%20%3D%20'https%3A%2F%2Fsimpledex.pages.dev'%0A%0A%20%20%20%20if%20(url.origin%20!%3D%3D%20mangadex)%20%7B%0A%20%20%20%20%20%20%20%20alert(%60Not%20a%20%24%7Bmangadex%7D%20URL%60)%0A%20%20%20%20%20%20%20%20return%0A%20%20%20%20%7D%0A%0A%20%20%20%20let%20dest%20%3D%20url.toString().replace(mangadex%2C%20simpledex)%0A%20%20%20%20window.location%20%3D%20dest%3B%0A%7D)()%3B%7D)()%3B
```

### Source:

```js
// Convert to bookmarklet with https://caiorss.github.io/bookmarklet-maker/
(function () {
    let url = new URL(document.location.href);
    let mangadex = 'https://mangadex.org'
    let simpledex = 'https://simpledex.pages.dev'

    if (url.origin !== mangadex) {
        alert(`Not a ${mangadex} URL`)
        return
    }

    let dest = url.toString().replace(mangadex, simpledex)
    window.location = dest;
})();
```
