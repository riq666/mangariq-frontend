import { merge } from 'webpack-merge'
import { commonConfig, distApiDir, srcApiDir } from './webpack.common'
import { Configuration } from 'webpack'

// ----------------------------------------------------------------------------
// Server
// ----------------------------------------------------------------------------

export default ((): Configuration => merge(commonConfig, {
    target: 'webworker',

    entry: {
        proxy: `${srcApiDir}/proxy.ts`,
    },

    output: {
        path: distApiDir,
    },
}))()
