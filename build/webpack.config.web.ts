import path from 'path'
import { merge } from 'webpack-merge'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import { staticDir, srcWebDir, distWebDir, distWebPublicDir, publicPath, commonWebConfig } from './webpack.common'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import { createOutputNameFn } from './createOutputNameFn'
import { Configuration } from 'webpack'
import 'webpack-dev-server'

// ----------------------------------------------------------------------------
// Web
// ----------------------------------------------------------------------------

export default ((): Configuration => merge(commonWebConfig, {
    entry: {
        main: path.resolve(srcWebDir, 'entryClient.ts'),
    },

    output: {
        path: distWebPublicDir,
        publicPath,
        filename: createOutputNameFn('js', true),
        chunkFilename: createOutputNameFn('js', false),
    },

    optimization: {
        chunkIds: 'named',
        splitChunks: {
            chunks: 'all',
            minSize: 0,
        },
    },

    devServer: {
        devMiddleware: {
            index: 'index.html',
            writeToDisk: (filePath) => {
                // Since output.publicPath is '/public', app.html can only be accessed at /public/index.html
                // Instead, we need to write it to disk and have webpack-dev-server serve it from '/' (contentBasePublicPath)
                return filePath.endsWith('.html')
            },
        },
        historyApiFallback: true,
        static: [
            {
                directory: distWebDir,
                publicPath: '/',
            },
            {
                directory: staticDir,
                publicPath: '/',
            },
        ],
    },

    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: staticDir,
                    to: distWebDir,
                },
            ],
        }),
        new MiniCssExtractPlugin({
            filename: createOutputNameFn('css', true),
            chunkFilename: createOutputNameFn('css', false),
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(srcWebDir, 'index.html'),
            filename: path.resolve(distWebDir, 'index.html'),
        }),
    ],
}))()
