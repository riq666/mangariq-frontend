import { merge } from 'webpack-merge'
import { commonConfig, distWebDir, srcSwDir } from './webpack.common'
import { Configuration } from 'webpack'

// ----------------------------------------------------------------------------
// Server
// ----------------------------------------------------------------------------

export default ((): Configuration => merge(commonConfig, {
    target: 'webworker',

    entry: {
        serviceWorker: `${srcSwDir}/serviceWorker.ts`,
    },

    output: {
        path: distWebDir,
    },
}))()
