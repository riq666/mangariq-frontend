import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { AttributeType } from './schemas/Result'
import { Tag } from './schemas/Tag'
import { fetchPaginatedResources } from './utils/fetchPaginatedResources'

function getTagsEndpoint() {
    return `${PROXY_API_URL}/manga/tag`
}

export async function purgeCachedTags(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await cache.delete(getTagsEndpoint())
}

export async function fetchTags(): Promise<Array<Tag>> {
    const response = await fetchPaginatedResources<AttributeType.Tag>(getTagsEndpoint)
    return response.data
}
