import axios from 'axios'
import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { Result } from './schemas/Result'
import { purgeCachedFollowStatus } from './follows'

function geMangaIsFollowedEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/user/follows/manga/${mangaId}`
}

function getMarkMangaAsFollowedEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/manga/${mangaId}/follow`
}

export async function purgeCachedMangaIsFollowed(mangaId: string): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await Promise.all([
        cache.delete(geMangaIsFollowedEndpoint(mangaId)),
        cache.delete(getMarkMangaAsFollowedEndpoint(mangaId)),
    ])
}

export async function fetchIsMangaFollowed(accessToken: string, mangaId: string): Promise<boolean> {
    try {
        const url = geMangaIsFollowedEndpoint(mangaId)
        await axios.get<Result>(url, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        })

        return true
    } catch (err) {
        if (axios.isAxiosError(err) && err.response?.status === 404) {
            return false
        }

        throw err
    }
}

export async function fetchMarkMangaAsFollowed(accessToken: string, mangaId: string, markAsFollowed: boolean): Promise<void> {
    const url = getMarkMangaAsFollowedEndpoint(mangaId)
    const headers = {
        Authorization: `Bearer ${accessToken}`,
    }

    if (markAsFollowed) {
        await axios.post(url, {}, { headers })
    } else {
        await axios.delete(url, { headers })
    }

    await purgeCachedFollowStatus(mangaId)
}
