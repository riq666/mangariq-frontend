import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { Chapter, ChapterQuery } from './schemas/Chapter'
import queryString from 'query-string'
import axios from 'axios'
import { AttributeType, PaginatedResult } from './schemas/Result'
import { FETCH_CHAPTERS_BASE_QUERY } from './chapter'
import { ContentRating } from './schemas/Manga'

function getMangaFeedEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/manga/${mangaId}/feed`
}

export async function purgeCachedMangaFeed(mangaId: string): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await cache.delete(getMangaFeedEndpoint(mangaId), { ignoreSearch: true })
}

export async function fetchMangaFeed(mangaId: string, offset = 0): Promise<Array<Chapter>> {
    const limit = 500
    const query: ChapterQuery = {
        ...FETCH_CHAPTERS_BASE_QUERY,
        contentRating: Object.values(ContentRating),
        limit,
        offset,
    }

    const url = queryString.stringifyUrl({
        url: getMangaFeedEndpoint(mangaId),
        query,
    }, {
        arrayFormat: 'bracket',
    })

    const res = await axios.get<PaginatedResult<AttributeType.Chapter>>(url)

    // Check if there are more pages
    if (res.data.offset + res.data.limit < res.data.total) {
        return [
            ...res.data.data,
            ...await fetchMangaFeed(mangaId, offset + limit),
        ]
    } else {
        return res.data.data
    }
}
