import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { AttributeType, PaginatedResult } from './schemas/Result'
import { purgeCachedMangaReadingStatus } from './followsReadingStatus'
import { purgeCachedMangaIsFollowed } from './followsIsFollowed'
import { FETCH_CHAPTERS_BASE_QUERY } from './chapter'
import { fetchPaginatedResources } from './utils/fetchPaginatedResources'
import { FollowsChapterFeedQuery, FollowsMangaListQuery } from './schemas/Follows'
import { FETCH_MANGA_BASE_QUERY } from './manga'

function getUserFollowsEndpoint() {
    return `${PROXY_API_URL}/user/follows/manga`
}

function getUserFollowsFeedEndpoint() {
    return `${PROXY_API_URL}/user/follows/manga/feed`
}

export async function purgeCachedUserFollows(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await cache.delete(getUserFollowsEndpoint(), { ignoreSearch: true })
}

export async function purgeCachedUserFollowsFeed(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await cache.delete(getUserFollowsFeedEndpoint(), { ignoreSearch: true })
}

export async function purgeCachedFollowStatus(mangaId: string): Promise<void> {
    await Promise.all([
        purgeCachedUserFollows(),
        purgeCachedUserFollowsFeed(),
        purgeCachedMangaIsFollowed(mangaId),
        purgeCachedMangaReadingStatus(mangaId),
    ])
}

export async function fetchUserFollows(accessToken: string, queryOverrides?: FollowsMangaListQuery): Promise<PaginatedResult<AttributeType.Manga>> {
    const query: FollowsMangaListQuery = {
        ...FETCH_MANGA_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchPaginatedResources(getUserFollowsEndpoint, query, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })
}

export async function fetchUserFollowsFeed(accessToken: string, queryOverrides?: FollowsChapterFeedQuery): Promise<PaginatedResult<AttributeType.Chapter>> {
    const query: FollowsChapterFeedQuery = {
        ...FETCH_CHAPTERS_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchPaginatedResources(getUserFollowsFeedEndpoint, query, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })
}
