import { PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'
import { Result } from './schemas/Result'

// ----------------------------------------------------------------------------
// Login
// ----------------------------------------------------------------------------

export interface LoginPayload {
    username: string
    password: string
}

export type LoginResponse = Result & {
    token: {
        session: string
        refresh: string
    }
}

export async function fetchLogin(payload: LoginPayload): Promise<LoginResponse> {
    const res = await axios.post<LoginResponse>(`${PROXY_API_URL}/auth/login`, payload)
    return res.data
}

// ----------------------------------------------------------------------------
// Logout
// ----------------------------------------------------------------------------

export type LogoutResponse = Result

export async function fetchLogout(accessToken: string): Promise<void> {
    await axios.post<LogoutResponse>(`${PROXY_API_URL}/auth/logout`, {}, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })
}

// ----------------------------------------------------------------------------
// Refresh
// ----------------------------------------------------------------------------

export type RefreshResponse = LoginResponse & {
    message: string
}

export async function fetchRefreshToken(refreshToken: string): Promise<LoginResponse> {
    const res = await axios.post<RefreshResponse>(`${PROXY_API_URL}/auth/refresh`, {
        token: refreshToken,
    })

    return res.data
}

// ----------------------------------------------------------------------------
// Check
// ----------------------------------------------------------------------------

export type UserCheckResponse = Result & {
    isAuthenticated: boolean
    roles: Array<string>
    permission: Array<string>
}

export async function fetchUserCheck(accessToken?: string): Promise<UserCheckResponse> {
    const res = await axios.get<UserCheckResponse>(`${PROXY_API_URL}/auth/check`, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })

    return res.data
}
