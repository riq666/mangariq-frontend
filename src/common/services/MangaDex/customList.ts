import { PROXY_API_URL } from '@/common/Constants'
import { purgeCache } from './utils/purgeCache'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { CustomList } from './schemas/CustomList'
import { AttributeType } from './schemas/Result'

function getListsEndpoint() {
    return `${PROXY_API_URL}/list`
}

function getListEndpoint(listId: string) {
    return `${PROXY_API_URL}/list/${listId}`
}

export async function purgeCachedList(listId?: string): Promise<void> {
    await purgeCache(getListsEndpoint, getListEndpoint, listId)
}

export async function fetchListsByIds(listIds: Array<string>): Promise<Array<CustomList>> {
    return await fetchResourcesByIds<AttributeType.CustomList>(getListsEndpoint, getListEndpoint, listIds)
}
