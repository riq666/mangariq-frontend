import { PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'
import queryString from 'query-string'
import { Result } from './schemas/Result'
import { purgeCache } from './utils/purgeCache'

type ReadChaptersResult = Result & {
    data: Array<string>
}

function getMangasReadChaptersEndpoint() {
    return `${PROXY_API_URL}/manga/read`
}

function getMangaReadChaptersEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/manga/${mangaId}/read`
}

export async function purgeCachedMangaRead(mangaId: string): Promise<void> {
    await purgeCache(getMangasReadChaptersEndpoint, getMangaReadChaptersEndpoint, mangaId)
}

export async function fetchMangasReadChapters(accessToken: string, mangaIds: Array<string>): Promise<Array<string>> {
    if (mangaIds.length === 0) {
        return []
    }

    // Use different endpoint in singular case for better caching
    // Do not use fetchResourcesByIds since this endpoint returns { data: string[] } instead of { results: string[] }
    if (mangaIds.length === 1) {
        const url = getMangaReadChaptersEndpoint(mangaIds[0])
        const res = await axios.get<ReadChaptersResult>(url, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        })

        return res.data.data
    } else {
        const url = queryString.stringifyUrl({
            url: getMangasReadChaptersEndpoint(),
            query: {
                ids: mangaIds,
            },
        }, {
            arrayFormat: 'bracket',
        })

        const res = await axios.get<ReadChaptersResult>(url, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        })

        return res.data.data
    }
}

export async function fetchMarkChaptersAsRead(accessToken: string, mangaId: string, chapterIds: Array<string>, markAsRead: boolean): Promise<void> {
    if (chapterIds.length === 0) {
        return
    }

    const MAX_MARKERS_PER_REQUEST = 200 // 10kb limit ~= 200 uuids
    const url = getMangaReadChaptersEndpoint(mangaId)
    const headers = {
        Authorization: `Bearer ${accessToken}`,
    }

    for (let i = 0; i < chapterIds.length; i += MAX_MARKERS_PER_REQUEST) {
        const body = markAsRead
            ? { chapterIdsRead: chapterIds.slice(i, i + MAX_MARKERS_PER_REQUEST) }
            : { chapterIdsUnread: chapterIds.slice(i, i + MAX_MARKERS_PER_REQUEST) }

        await axios.post(url, body, { headers })
    }

    await purgeCachedMangaRead(mangaId)
}
