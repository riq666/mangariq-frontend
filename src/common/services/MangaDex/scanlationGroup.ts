import { PROXY_API_URL } from '@/common/Constants'
import { ScanlationGroup } from './schemas/ScanlationGroup'
import { purgeCache } from './utils/purgeCache'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { AttributeType } from './schemas/Result'

function getScanlationGroupsEndpoint() {
    return `${PROXY_API_URL}/group`
}

function getScanlationGroupEndpoint(groupId: string) {
    return `${PROXY_API_URL}/group/${groupId}`
}

export async function purgeCachedScanlationGroup(groupId?: string): Promise<void> {
    await purgeCache(getScanlationGroupsEndpoint, getScanlationGroupEndpoint, groupId)
}

export async function fetchScanlationGroups(groupIds: Array<string>): Promise<Array<ScanlationGroup>> {
    return await fetchResourcesByIds<AttributeType.ScanlationGroup>(getScanlationGroupsEndpoint, getScanlationGroupEndpoint, groupIds)
}
