import { pascalCaseToSpaced } from '@/common/utils/pascalCaseToSpaced'
import { ExternalSite } from './ExternalSite'
import { Language, LocalizedString } from './Language'
import { AttributeType, DataResult } from './Result'
import { SortOrder } from './SortOrder'
import { Tag, TagSearchMode } from './Tag'

// ----------------------------------------------------------------------------
// Manga Attributes
// ----------------------------------------------------------------------------

export enum PublicationDemographic {
    Shounen = 'shounen',
    Shoujo = 'shoujo',
    Josei = 'josei',
    Seinen = 'seinen',
}

export const PUBLICATION_DEMOGRAPHIC_OPTIONS = Object.entries(PublicationDemographic).map(([label, value]) => ({
    label: pascalCaseToSpaced(label),
    value,
}))

export enum MangaStatus {
    Ongoing = 'ongoing',
    Completed = 'completed',
    Hiatus = 'hiatus',
    Cancelled = 'cancelled',
}

export const MANGA_STATUS_OPTIONS = Object.entries(MangaStatus).map(([label, value]) => ({
    label: pascalCaseToSpaced(label),
    value,
}))

export enum ContentRating {
    Safe = 'safe',
    Suggestive = 'suggestive',
    Erotica = 'erotica',
    Pornographic = 'pornographic',
}

export const CONTENT_RATING_OPTIONS = Object.entries(ContentRating).map(([label, value]) => ({
    label: pascalCaseToSpaced(label),
    value,
}))

export enum ReadingStatus {
    Reading = 'reading',
    OnHold = 'on_hold',
    PlanToRead = 'plan_to_read',
    Dropped = 'dropped',
    Rereading = 're_reading',
    Completed = 'completed',
}

export const READING_STATUS_OPTIONS = Object.entries(ReadingStatus).map(([label, value]) => ({
    label: pascalCaseToSpaced(label),
    value,
}))

export enum MangaRelationshipType {
    Monochrome = 'monochrome',
    Colored = 'colored',

    Preserialization = 'preserialization',
    Serialization = 'serialization',

    Prequel = 'prequel',
    Sequel = 'sequel',
    MainStory = 'main_story',
    SideStory = 'side_story',

    AdaptedFrom = 'adapted_from',
    SpinOff = 'spin_off',
    BasedOn = 'based_on',
    Doujinshi = 'doujinshi',

    SameFranchise = 'same_franchise',
    SharedUniverse = 'shared_universe',
    AlternateStory = 'alternate_story',
    AlternateVersion = 'alternate_version',
}

export const MANGA_RELATIONSHIP_TYPE_OPTIONS = Object.entries(MangaRelationshipType).map(([label, value]) => ({
    label: pascalCaseToSpaced(label),
    value,
}))

// ----------------------------------------------------------------------------
// Manga
// ----------------------------------------------------------------------------

export interface MangaAttributes {
    title: LocalizedString
    altTitles: Array<LocalizedString>
    description: LocalizedString
    originalLanguage: Language
    lastVolume: string | null
    lastChapter: string | null
    publicationDemographic: PublicationDemographic | null
    status: MangaStatus | null
    year: number | null
    contentRating: ContentRating | null
    tags: Array<Tag>
    links: Partial<Record<ExternalSite, string>> | null

    version: number
    createdAt: string
    updatedAt: string
}

export type Manga = DataResult<AttributeType.Manga>['data']

export type MangaQuery = Partial<{
    limit: number
    offset: number
    ids: Array<string>
    includes: Array<AttributeType>

    'order[title]': SortOrder
    'order[year]': SortOrder
    'order[createdAt]': SortOrder
    'order[updatedAt]': SortOrder
    'order[latestUploadedChapter]': SortOrder
    'order[followedCount]': SortOrder
    'order[relevance]': SortOrder

    title: string
    authors: Array<string>
    artists: Array<string>
    year: number
    includedTags: Array<string>
    includedTagsMode: TagSearchMode
    excludedTags: Array<string>
    excludedTagsMode: TagSearchMode
    status: Array<MangaStatus>
    originalLanguage: Array<Language>
    publicationDemographic: Array<PublicationDemographic>
    contentRating: Array<ContentRating>
    createdAtSince: string
    updatedAtSince: string
    hasAvailableChapters: '0' | '1'
}>

export type MangaEditRequest = Omit<MangaAttributes,
    'createdAt' |
    'updatedAt'
> & {
    authors: Array<string>
    artists: Array<string>
    modNotes: string | null
}

// ----------------------------------------------------------------------------
// Default Search Filters
// ----------------------------------------------------------------------------

export const DEFAULT_ORIGINAL_LANGUAGE = Object.values(Language)

export const DEFAULT_MANGA_STATUS = Object.values(MangaStatus)

export const DEFAULT_CONTENT_RATING = [
    ContentRating.Safe,
    ContentRating.Suggestive,
    ContentRating.Erotica,
]

export const DEFAULT_PUBLICATION_DEMOGRAPHIC = Object.values(PublicationDemographic)

export const DEFAULT_INCLUDE_TAG_MODE = TagSearchMode.All

export const DEFAULT_EXCLUDE_TAG_MODE = TagSearchMode.Any
