import { AttributeType, DataResult } from './Result'

export enum CustomListVisibility {
    Private = 'private',
    Public = 'public',
}

export interface CustomListAttributes {
    name: string
    visibility: CustomListVisibility

    version: number
}

export type CustomList = DataResult<AttributeType.CustomList>['data']
