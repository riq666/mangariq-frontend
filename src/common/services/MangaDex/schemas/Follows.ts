import { ChapterQuery } from './Chapter'
import { MangaQuery } from './Manga'

export type FollowsMangaListQuery = Pick<MangaQuery,
    'limit' |
    'offset' |

    'includes'
>

export type FollowsChapterFeedQuery = Pick<ChapterQuery,
    'limit' |
    'offset' |

    'order[createdAt]' |
    'order[updatedAt]' |
    'order[publishAt]' |
    'order[volume]' |
    'order[chapter]' |

    'translatedLanguage' |
    'originalLanguage' |
    'excludedOriginalLanguage' |
    'contentRating' |
    'includeFutureUpdates' |

    'createdAtSince' |
    'updatedAtSince' |
    'publishAtSince' |

    'includes'
>
