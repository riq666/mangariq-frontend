export interface AtHome {
    baseUrl: string
    chapter: {
        hash: string
        data: Array<string>
        dataSaver: Array<string>
    }
}
