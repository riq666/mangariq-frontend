export enum OfficialSite {
    Amazon = 'amz',
    BookWalkerJp = 'bw',
    EbookJapan = 'ebj',
    EnglishLicensedSrc = 'engtl',
    Raw = 'raw',
}

export enum TrackingSite {
    AniList = 'al',
    AnimePlanet = 'ap',
    Kitsu = 'kt',
    MangaUpdates = 'mu',
    MyAnimeList = 'mal',
    NovelUpdates = 'nu',
}

export type ExternalSite = OfficialSite | TrackingSite

export const EXTERNAL_SITE_OPTIONS = [
    ...Object.values(OfficialSite).map((site) => ({
        label: getSourceName(site),
        value: site,
    })),
    ...Object.values(TrackingSite).map((site) => ({
        label: getSourceName(site),
        value: site,
    })),
]

export function getSourceName(src: ExternalSite): string {
    switch (src) {
        case OfficialSite.Amazon: {
            return 'Amazon Japan'
        }
        case OfficialSite.BookWalkerJp: {
            return 'BookWalker Japan'
        }
        case OfficialSite.EbookJapan: {
            return 'Ebook Japan'
        }
        case OfficialSite.EnglishLicensedSrc: {
            return 'Official English'
        }
        case OfficialSite.Raw: {
            return 'Raw'
        }
        case TrackingSite.AniList: {
            return 'AniList'
        }
        case TrackingSite.AnimePlanet: {
            return 'Anime Planet'
        }
        case TrackingSite.Kitsu: {
            return 'Kitsu'
        }
        case TrackingSite.MangaUpdates: {
            return 'Manga Updates'
        }
        case TrackingSite.MyAnimeList: {
            return 'MyAnimeList'
        }
        case TrackingSite.NovelUpdates: {
            return 'Novel Updates'
        }
        default: {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            throw new Error(`Source "${src}" not implemented`)
        }
    }
}

export function getSourceUrl(src: ExternalSite, val: string): string {
    switch (src) {
        case OfficialSite.Amazon: {
            return val
        }
        case OfficialSite.BookWalkerJp: {
            return `https://bookwalker.jp/${val}`
        }
        case OfficialSite.EbookJapan: {
            return val
        }
        case OfficialSite.EnglishLicensedSrc: {
            return val
        }
        case OfficialSite.Raw: {
            return val
        }
        case TrackingSite.AniList: {
            return `https://anilist.co/manga/${val}`
        }
        case TrackingSite.AnimePlanet: {
            return `https://www.anime-planet.com/manga/${val}`
        }
        case TrackingSite.Kitsu: {
            return `https://kitsu.io/manga/${val}`
        }
        case TrackingSite.MangaUpdates: {
            return `https://www.mangaupdates.com/series.html?id=${val}`
        }
        case TrackingSite.MyAnimeList: {
            return `https://myanimelist.net/manga/${val}`
        }
        case TrackingSite.NovelUpdates: {
            return `https://www.novelupdates.com/series/${val}`
        }
        default: {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            throw new Error(`Source "${src}" not implemented`)
        }
    }
}
