import { Language } from './Language'
import { ContentRating } from './Manga'
import { AttributeType, DataResult } from './Result'
import { SortOrder } from './SortOrder'

export interface ChapterAttributes {
    title: string
    volume: string
    chapter: string | null
    translatedLanguage: string
    externalUrl: string | null

    version: number
    createdAt: string
    updatedAt: string
    publishAt: string
}

export type Chapter = DataResult<AttributeType.Chapter>['data']

export function getChapterTitle(chapter?: Chapter): string {
    if (!chapter) {
        return ''
    }

    let name = chapter.attributes.chapter
        ? `Chapter ${chapter.attributes.chapter}`
        : 'One Shot'

    const title = chapter.attributes.title
    if (title) {
        name += `: ${title}`
    }

    return name
}

export type ChapterQuery = Partial<{
    limit: number
    offset: number
    ids: Array<string>
    includes: Array<AttributeType>

    'order[createdAt]': SortOrder
    'order[updatedAt]': SortOrder
    'order[publishAt]': SortOrder
    'order[volume]': SortOrder
    'order[chapter]': SortOrder

    title: string
    groups: Array<string>
    uploader: string
    manga: string
    volume: Array<string>
    chapter: Array<string>
    translatedLanguage: Array<Language>
    originalLanguage: Array<Language>
    excludedOriginalLanguage: Array<Language>
    contentRating: Array<ContentRating>
    includeFutureUpdates: '0' | '1'

    createdAtSince: string
    updatedAtSince: string
    publishAtSince: string
}>
