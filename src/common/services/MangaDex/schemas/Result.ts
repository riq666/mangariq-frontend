import { AuthorAttributes } from './Author'
import { ChapterAttributes } from './Chapter'
import { CoverAttributes } from './Cover'
import { CustomListAttributes } from './CustomList'
import { MangaAttributes, MangaRelationshipType } from './Manga'
import { ScanlationGroupAttributes } from './ScanlationGroup'
import { TagAttributes } from './Tag'

export enum AttributeType {
    CoverArt = 'cover_art',
    Manga = 'manga',
    ScanlationGroup = 'scanlation_group',
    Chapter = 'chapter',
    CustomList = 'custom_list',
    Tag = 'tag',
    Author = 'author',
    Artist = 'artist',
}

interface AttributeMap {
    [AttributeType.CoverArt]: CoverAttributes
    [AttributeType.ScanlationGroup]: ScanlationGroupAttributes
    [AttributeType.Manga]: MangaAttributes
    [AttributeType.Chapter]: ChapterAttributes
    [AttributeType.CustomList]: CustomListAttributes
    [AttributeType.Tag]: TagAttributes
    [AttributeType.Author]: AuthorAttributes
    [AttributeType.Artist]: AuthorAttributes
}

export type Relationship<T extends keyof AttributeMap> = {
    id: string
    type: T
    attributes?: AttributeMap[T]
    related?: MangaRelationshipType
}

export type Result = {
    result: 'ok' | 'error'
}

export type DataResult<T extends keyof AttributeMap> = Result & {
    response: 'entity'
    data: {
        id: string
        type: T
        attributes: AttributeMap[T]
        relationships?: Array<Relationship<AttributeType>>
    }
}

export type PaginatedResult<T extends keyof AttributeMap> = {
    response: 'collection'
    data: Array<DataResult<T>['data']>
    limit: number
    offset: number
    total: number
}

export type ErrorResult = Result & {
    errors: Array<{
        detail: string
        id: string
        status: number
        title: string
    }>
}

export function findRelationship<D extends keyof AttributeMap, K extends keyof AttributeMap>(result: DataResult<D>['data'] | undefined, relationshipType: K): Relationship<K> | undefined {
    for (const relationship of result?.relationships ?? []) {
        if (relationship.type === relationshipType) {
            return relationship as Relationship<K>
        }
    }

    return undefined
}

export function findAllRelationships<D extends keyof AttributeMap, K extends keyof AttributeMap>(result: DataResult<D>['data'] | undefined, relationshipType: K): Array<Relationship<K>> {
    const relationships: Array<Relationship<K>> = []

    for (const relationship of result?.relationships ?? []) {
        if (relationship.type !== relationshipType) {
            continue
        }

        relationships.push(relationship as Relationship<K>)
    }

    return relationships
}

export function mapRelationships<D extends keyof AttributeMap, K extends keyof AttributeMap>(results: Array<DataResult<D>['data']>, relationshipType: K, throwOnMissing = true): Array<Relationship<K>> {
    const relationships = []

    for (const result of results) {
        const relationship = findRelationship(result, relationshipType)
        if (!relationship) {
            if (throwOnMissing) {
                throw new Error(`Failed to find relationship:${relationshipType} for ${JSON.stringify(result)}`)
            } else {
                continue
            }
        }

        relationships.push(relationship)
    }

    return relationships
}
