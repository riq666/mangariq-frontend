import { Chapter } from './Chapter'
import { AttributeType, findRelationship } from './Result'

export interface ChapterGroup {
    key: string
    chapterIds: Array<string>
}

export function createChapterComparator(): (a: Chapter, b: Chapter) => number {
    return (a, b) => {
        const aChapter = parseFloat(a.attributes.chapter ?? '0')
        const bChapter = parseFloat(b.attributes.chapter ?? '0')
        if (aChapter !== bChapter) {
            return (bChapter - aChapter)
        }

        const aPublishAt = a.attributes.publishAt
        const bPublishAt = b.attributes.publishAt
        if (aPublishAt !== bPublishAt) {
            return bPublishAt.localeCompare(aPublishAt)
        }

        return 0
    }
}

export function groupChaptersByChapter(chapters: Array<Chapter>): Array<ChapterGroup> {
    const sortedChapters = chapters.sort(createChapterComparator())

    const groupedChapters: Array<ChapterGroup> = []
    let prevKey: string | undefined

    for (const chapter of sortedChapters) {
        const currentKey = chapter.attributes.chapter ?? '0'

        if (prevKey !== currentKey) {
            groupedChapters.push({
                key: currentKey,
                chapterIds: [],
            })
        }

        groupedChapters[groupedChapters.length - 1].chapterIds.push(chapter.id)
        prevKey = currentKey
    }

    return groupedChapters
}

export function groupChaptersByManga(chapters: Array<Chapter>): Array<ChapterGroup> {
    const groupedChapters = new Map<string, ChapterGroup>()

    for (const chapter of chapters) {
        const currentKey = findRelationship(chapter, AttributeType.Manga)?.id
        if (!currentKey) {
            throw new Error(`Missing manga relationship for chapter:${chapter.id}`)
        }

        if (!groupedChapters.has(currentKey)) {
            groupedChapters.set(currentKey, {
                key: currentKey,
                chapterIds: [],
            })
        }

        groupedChapters.get(currentKey)?.chapterIds.push(chapter.id)
    }

    return [...groupedChapters.values()]
}
