import { AttributeType, DataResult } from './Result'

export interface CoverAttributes {
    volume: string | null
    fileName: string
    description: string | null

    version: number
    createdAt: string
    updatedAt: string
}

export type Cover = DataResult<AttributeType.CoverArt>['data']

export function getCoverUrl(mangaId: string, coverAttributes?: CoverAttributes): string | undefined {
    if (!coverAttributes) {
        return undefined
    }

    const coverFileName = coverAttributes.fileName
    return `https://uploads.mangadex.org/covers/${mangaId}/${coverFileName}.256.jpg`
}
