import { AttributeType, DataResult } from './Result'
import { SortOrder } from './SortOrder'

export interface AuthorAttributes {
    name: string
    imageUrl: string | null
    biography: Array<never>

    version: number
    createdAt: string
    updatedAt: string
}

export type Author = DataResult<AttributeType.Author>['data']

export type Artist = DataResult<AttributeType.Artist>['data']

export type AuthorQuery = Partial<{
    limit: number
    offset: number
    ids: Array<string>
    includes: Array<AttributeType>

    name: string
    'order[name]': SortOrder
}>
