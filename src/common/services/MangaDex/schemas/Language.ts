export enum Language {
    English = 'en',
    Japanese = 'ja',
    Chinese = 'zh',
    Korean = 'ko',
}

export type LocalizedString = Partial<Record<Language, string>>

export const LANGUAGE_OPTIONS = Object.entries(Language).map(([label, value]) => ({
    label,
    value,
}))
