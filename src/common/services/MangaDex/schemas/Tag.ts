import { LocalizedString } from './Language'
import { AttributeType, DataResult } from './Result'

export interface TagAttributes {
    name: LocalizedString
    description: LocalizedString
    group: string
    version: number
}

export type Tag = DataResult<AttributeType.Tag>['data']

export enum TagSearchMode {
    All = 'AND',
    Any = 'OR',
}

export const TAG_SEARCH_MODE_OPTIONS = Object.entries(TagSearchMode).map(([label, value]) => ({
    label,
    value,
}))
