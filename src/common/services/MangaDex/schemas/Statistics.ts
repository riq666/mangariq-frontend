import { Result } from './Result'

export interface SingleMangaStatistic {
    rating: {
        average: number
        distribution?: Record<number, number>
    }
    follows: number
}

export type MangaStatistic = Result & {
    statistics: Record<string, SingleMangaStatistic>
}
