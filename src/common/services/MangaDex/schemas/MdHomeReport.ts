export interface MdHomeReport {
    url: string
    success: boolean
    cached: boolean
    bytes: number
    duration: number
}
