import { AttributeType, DataResult } from './Result'

export interface ScanlationGroupAttributes {
    name: string
    website: string | null
    ircServer: string | null
    ircChanel: string | null
    discord: string | null
    contactEmail: string | null
    description: string | null
    locked: boolean
    official: boolean

    version: number
    createdAt: string
    updatedAt: string
}

export type ScanlationGroup = DataResult<AttributeType.ScanlationGroup>['data']
