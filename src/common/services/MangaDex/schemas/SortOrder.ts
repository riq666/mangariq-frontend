export enum SortOrder {
    Ascending = 'asc',
    Descending = 'desc',
}

export const SORT_ORDER_OPTIONS = Object.entries(SortOrder).map(([label, value]) => ({
    label,
    value,
}))
