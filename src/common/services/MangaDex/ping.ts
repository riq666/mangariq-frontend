import { MD_API_URL, PING_TIMEOUT, PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'

export async function fetchPingProxy(): Promise<boolean> {
    return await pingServer(PROXY_API_URL)
}

export async function fetchPingMangaDex(): Promise<boolean> {
    return await pingServer(MD_API_URL)
}

async function pingServer(origin: string): Promise<boolean> {
    const timeout = axios.CancelToken.source()
    const timeoutId = setTimeout(() => timeout.cancel(`Request timeout: ${origin}`), PING_TIMEOUT)
    let success = false

    try {
        await axios.get(`${origin}/ping`, { cancelToken: timeout.token })
        success = true
    } catch (err) {
        console.warn(err)
    } finally {
        clearTimeout(timeoutId)
    }

    return success
}
