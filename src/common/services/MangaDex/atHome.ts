import { MD_NETWORK_API_URL, PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'
import { Chapter } from './schemas/Chapter'
import queryString from 'query-string'
import { MdHomeReport } from './schemas/MdHomeReport'
import { AtHome } from './schemas/AtHome'

export enum QualityMode {
    Original = 'data',
    DataSaver = 'data-saver',
}

function getFindAtHomeServerEndpoint(chapterId: string) {
    return `${PROXY_API_URL}/at-home/server/${chapterId}`
}

function getAtHomeServerReportEndpoint() {
    return `${MD_NETWORK_API_URL}/report`
}

export async function fetchChapterPageUrls(chapter: Chapter, qualityMode = QualityMode.DataSaver): Promise<Array<string>> {
    const url = queryString.stringifyUrl({
        url: getFindAtHomeServerEndpoint(chapter.id),
        query: {
            forcePort443: true,
        },
    })

    const res = await axios.get<AtHome>(url)

    const baseUrl = res.data.baseUrl
    const chapterHash = res.data.chapter.hash
    let pageUrls: Array<string>

    switch (qualityMode) {
        case QualityMode.Original: {
            pageUrls = res.data.chapter.data.map((fileName) => `${baseUrl}/${qualityMode}/${chapterHash}/${fileName}`)
            break
        }
        case QualityMode.DataSaver: {
            pageUrls = res.data.chapter.dataSaver.map((fileName) => `${baseUrl}/${qualityMode}/${chapterHash}/${fileName}`)
            break
        }
        default: {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            throw new Error(`qualityMode:${qualityMode} not implemented`)
        }
    }

    void validatePageUrls(pageUrls)
    return pageUrls
}

async function validatePageUrls(pageUrls: Array<string>): Promise<void> {
    const requests: Array<Promise<void>> = []

    for (const pageUrl of pageUrls) {
        if (!new URL(pageUrl).origin.includes('mangadex.network')) {
            continue
        }

        requests.push((async() => {
            let success = false
            let bytes = 0
            let duration = 0
            let cached = false

            try {
                const startTime = new Date()
                const res = await axios.get<Blob>(pageUrl, { responseType: 'blob' })
                const endTime = new Date()

                success = true
                bytes = res.data.size
                duration = endTime.getTime() - startTime.getTime()
                cached = (res.headers as Record<string, string>)['x-cache'] === 'HIT'
            } catch (err) {
                console.warn(err)
            }

            const report: MdHomeReport = {
                url: pageUrl,
                success,
                bytes,
                duration,
                cached,
            }

            await axios.post(getAtHomeServerReportEndpoint(), report)
        })())
    }

    await Promise.all(requests)
}
