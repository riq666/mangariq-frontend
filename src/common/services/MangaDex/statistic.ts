import { MAX_IDS, PROXY_API_URL } from '@/common/Constants'
import { MangaStatistic } from './schemas/Statistics'
import { purgeCache } from './utils/purgeCache'
import queryString from 'query-string'
import axios from 'axios'

function getStatisticsEndpoint() {
    return `${PROXY_API_URL}/statistics/manga`
}

function getStatisticEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/statistics/manga/${mangaId}`
}

export async function purgeCachedMangaStatistic(mangaId?: string): Promise<void> {
    await purgeCache(getStatisticsEndpoint, getStatisticEndpoint, mangaId)
}

export async function fetchMangaStatisticsByIds(mangaIds: Array<string>): Promise<MangaStatistic['statistics']> {
    if (mangaIds.length < 1) {
        return {}
    }

    if (mangaIds.length === 1) {
        const url = getStatisticEndpoint(mangaIds[0])
        const res = await axios.get<MangaStatistic>(url)
        return res.data.statistics
    } else if (mangaIds.length < MAX_IDS) {
        const url = queryString.stringifyUrl({
            url: getStatisticsEndpoint(),
            query: {
                manga: mangaIds,
            },
        }, {
            arrayFormat: 'bracket',
        })
        const res = await axios.get<MangaStatistic>(url)
        return res.data.statistics
    } else {
        return {
            ...await fetchMangaStatisticsByIds(mangaIds.slice(0, MAX_IDS)),
            ...await fetchMangaStatisticsByIds(mangaIds.slice(MAX_IDS)),
        }
    }
}
