import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'
import { ReadingStatus } from './schemas/Manga'
import { Result } from './schemas/Result'

type MangaReadingStatusesResult = Result & {
    statuses: Record<string, ReadingStatus>
}

type MangaReadingStatusResult = Result & {
    status: ReadingStatus | null
}

function getMangasReadingStatusEndpoint() {
    return `${PROXY_API_URL}/manga/status`
}

function getMangaReadingStatusEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/manga/${mangaId}/status`
}

export async function purgeCachedMangaReadingStatus(mangaId: string): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    await Promise.all([
        cache.delete(getMangaReadingStatusEndpoint(mangaId)),
        cache.delete(getMangasReadingStatusEndpoint()),
    ])
}

export async function fetchMangasReadingStatus(accessToken: string): Promise<Record<string, ReadingStatus>> {
    const url = getMangasReadingStatusEndpoint()
    const res = await axios.get<MangaReadingStatusesResult>(url, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })

    return res.data.statuses
}

export async function fetchMangaReadingStatus(accessToken: string, mangaId: string): Promise<ReadingStatus | null> {
    const url = getMangaReadingStatusEndpoint(mangaId)
    const res = await axios.get<MangaReadingStatusResult>(url, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    })

    return res.data.status
}

export async function fetchSetMangaReadingStatus(accessToken: string, mangaId: string, readingStatus: ReadingStatus | null): Promise<void> {
    const url = getMangaReadingStatusEndpoint(mangaId)
    const body = {
        status: readingStatus,
    }
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
    }

    await axios.post(url, body, { headers })

    await purgeCachedMangaReadingStatus(mangaId)
}
