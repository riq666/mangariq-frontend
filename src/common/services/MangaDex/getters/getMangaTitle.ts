import { Language } from '../schemas/Language'
import { MangaAttributes } from '../schemas/Manga'

export function getMangaTitle(attrs?: MangaAttributes): string {
    const year = attrs?.year ?? 0
    const yearSuffix = year > 0
        ? ` (${year})`
        : ''

    const enTitle = attrs?.title[Language.English]
    if (enTitle) {
        return enTitle + yearSuffix
    }

    for (const altTitle of attrs?.altTitles ?? []) {
        for (const [language, titleString] of Object.entries(altTitle)) {
            if (language === Language.English) {
                return titleString + yearSuffix
            }
        }
    }

    const anyAltTitle = Object.values(attrs?.altTitles?.[0] ?? {})[0]
    return (anyAltTitle ?? 'Untitled Manga') + yearSuffix
}
