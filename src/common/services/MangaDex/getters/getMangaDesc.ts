import { Language } from '../schemas/Language'
import { MangaAttributes } from '../schemas/Manga'

export function getMangaDesc(attrs?: MangaAttributes): string {
    const enDesc = attrs?.description[Language.English]
    if (enDesc) {
        return enDesc
    }

    const anyDesc = Object.values(attrs?.description ?? {})[0]
    return anyDesc || 'No description available'
}
