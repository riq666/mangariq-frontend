import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { Author, AuthorQuery } from './schemas/Author'
import { AttributeType } from './schemas/Result'
import { purgeCache } from './utils/purgeCache'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { fetchPaginatedResources } from './utils/fetchPaginatedResources'

function getAuthorsEndpoint() {
    return `${PROXY_API_URL}/author`
}

function getAuthorEndpoint(authorId: string) {
    return `${PROXY_API_URL}/author/${authorId}`
}

export async function purgeCachedAuthors(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    const matchedResponses = (await cache.matchAll(getAuthorsEndpoint(), { ignoreSearch: true })).filter((res) => !res.url.includes('ids[]='))
    await Promise.all(matchedResponses.map((response) => cache.delete(response.url)))
}

export async function purgeCachedAuthor(authorId?: string): Promise<void> {
    await purgeCache(getAuthorsEndpoint, getAuthorEndpoint, authorId)
}

export const FETCH_AUTHORS_BASE_QUERY: AuthorQuery = {
    includes: [
        AttributeType.Manga,
    ],
}

export async function fetchAuthorsByIds(authorIds: Array<string>, queryOverrides?: AuthorQuery): Promise<Array<Author>> {
    const query: AuthorQuery = {
        ...FETCH_AUTHORS_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchResourcesByIds<AttributeType.Author>(getAuthorsEndpoint, getAuthorEndpoint, authorIds, query)
}

export async function fetchAuthorByName(name: string, queryOverrides?: AuthorQuery): Promise<Array<Author>> {
    const query: AuthorQuery = {
        ...FETCH_AUTHORS_BASE_QUERY,
        ...queryOverrides,
        limit: 10,
        name: name.toLocaleLowerCase(), // Case insensitive for better caching
    }

    const response = await fetchPaginatedResources<AttributeType.Author>(getAuthorsEndpoint, query)
    return response.data
}
