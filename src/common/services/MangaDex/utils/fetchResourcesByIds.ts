import { MAX_IDS } from '@/common/Constants'
import axios from 'axios'
import queryString, { StringifiableRecord } from 'query-string'
import { AttributeType, DataResult, PaginatedResult } from '../schemas/Result'

type GetQueryRoute = () => string
type GetSpecificRoute = (id: string) => string

export async function fetchResourcesByIds<T extends AttributeType>(getQueryRoute: GetQueryRoute, getSpecificRoute: GetSpecificRoute, ids: Array<string>, query?: StringifiableRecord): Promise<Array<DataResult<T>['data']>> {
    if (ids.length < 1) {
        return []
    }

    // Use different endpoint in singular case for better caching
    if (ids.length === 1) {
        const url = queryString.stringifyUrl({
            url: getSpecificRoute(ids[0]),
            query,
        }, {
            arrayFormat: 'bracket',
        })

        const res = await axios.get<DataResult<T>>(url)
        return [res.data.data]
    } else if (ids.length <= MAX_IDS) {
        const url = queryString.stringifyUrl({
            url: getQueryRoute(),
            query: {
                ids,
                limit: ids.length,
                ...query,
            },
        }, {
            arrayFormat: 'bracket',
        })

        const res = await axios.get<PaginatedResult<T>>(url)
        return res.data.data
    } else {
        return [
            ...await fetchResourcesByIds<T>(getQueryRoute, getSpecificRoute, ids.slice(0, MAX_IDS), query),
            ...await fetchResourcesByIds<T>(getQueryRoute, getSpecificRoute, ids.slice(MAX_IDS), query),
        ]
    }
}
