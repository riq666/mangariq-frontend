import { MD_API_CACHE_KEY } from '@/common/Constants'

export async function purgeCache(getQueryRoute: () => string, getSpecificRoute: (id: string) => string, id?: string): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    if (!id) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)

    await Promise.all([
        cache.delete(getSpecificRoute(id), { ignoreSearch: true }),
        (async() => {
            const matchedResponses = (await cache.matchAll(getQueryRoute(), { ignoreSearch: true })).filter((res) => res.url.includes(id))
            await Promise.all(matchedResponses.map((response) => cache.delete(response.url)))
        })(),
    ])
}
