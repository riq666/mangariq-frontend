import axios, { AxiosRequestConfig } from 'axios'
import queryString, { StringifiableRecord } from 'query-string'
import { AttributeType, PaginatedResult } from '../schemas/Result'

export async function fetchPaginatedResources<T extends AttributeType>(getQueryRoute: () => string, query?: StringifiableRecord, axiosConfig?: AxiosRequestConfig): Promise<PaginatedResult<T>> {
    const url = queryString.stringifyUrl({
        url: getQueryRoute(),
        query,
    }, {
        arrayFormat: 'bracket',
    })

    const res = await axios.get<PaginatedResult<T>>(url, axiosConfig)
    return res.data
}
