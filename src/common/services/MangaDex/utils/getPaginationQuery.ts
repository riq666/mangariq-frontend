import { MAX_IDS, MAX_RESULTS, QUERY_LIMIT } from '@/common/Constants'

interface PaginationQuery {
    offset: number
    limit: number
}

export function getPaginationQuery(page = 0, queryLimit = QUERY_LIMIT): PaginationQuery {
    const offset = page * queryLimit
    const limit = Math.min(MAX_RESULTS - offset, queryLimit)

    if (offset < 0 || limit < 0) {
        throw new Error(`Invalid offset:${offset} limit:${limit}`)
    }

    if (offset >= MAX_RESULTS) {
        throw new Error(`Invalid offset ${offset} >= ${MAX_RESULTS}`)
    }

    if (limit > MAX_IDS) {
        throw new Error(`Invalid limit ${limit} > ${MAX_IDS}`)
    }

    return {
        offset,
        limit,
    }
}
