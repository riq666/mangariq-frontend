import { PROXY_API_URL } from '@/common/Constants'
import { isUuid } from '@/common/utils/isUuid'
import axios from 'axios'
import { Author } from './schemas/Author'
import { Cover } from './schemas/Cover'
import { Manga, MangaEditRequest } from './schemas/Manga'
import { AttributeType, DataResult } from './schemas/Result'

export async function fetchCreateAuthor(accessToken: string, name: string): Promise<Author> {
    const data = {
        name,
        version: 1,
    }

    const res = await axios.post<DataResult<AttributeType.Author>>(`${PROXY_API_URL}/author`, data, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
    })

    return res.data.data
}

export async function fetchUploadCover(accessToken: string, mangaId: string, file: File): Promise<Cover> {
    const data = new FormData()
    data.append('file', file)

    const res = await axios.post<DataResult<AttributeType.CoverArt>>(`${PROXY_API_URL}/cover/${mangaId}`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${accessToken}`,
        },
    })

    return res.data.data
}

export async function fetchCreateManga(accessToken: string, mangaRequest: MangaEditRequest, coverArtFile: File | null): Promise<Manga> {
    const nonexistentCreators = new Set([
        ...mangaRequest.authors.filter((author) => !isUuid(author)),
        ...mangaRequest.artists.filter((artist) => !isUuid(artist)),
    ])

    const creatorIds = new Map<string, string>()
    for (const creatorName of nonexistentCreators) {
        const author = await fetchCreateAuthor(accessToken, creatorName)
        creatorIds.set(creatorName, author.id)
    }

    const data: MangaEditRequest = {
        ...mangaRequest,
        authors: mangaRequest.authors.map((author) => creatorIds.get(author) ?? author),
        artists: mangaRequest.artists.map((artist) => creatorIds.get(artist) ?? artist),
    }

    const res = await axios.post<DataResult<AttributeType.Manga>>(`${PROXY_API_URL}/manga`, data, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
    })

    if (coverArtFile) {
        const mangaId = res.data.data.id
        await fetchUploadCover(accessToken, mangaId, coverArtFile)
    }

    return res.data.data
}
