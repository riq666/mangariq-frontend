import { PROXY_API_URL } from '@/common/Constants'
import { Manga } from './schemas/Manga'
import { AttributeType, findRelationship, mapRelationships } from './schemas/Result'
import { purgeCache } from './utils/purgeCache'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { getCoverUrl } from './schemas/Cover'

function getCoversEndpoint() {
    return `${PROXY_API_URL}/cover`
}

function getCoverEndpoint(coverId: string) {
    return `${PROXY_API_URL}/cover/${coverId}`
}

export async function purgeCachedCoverUrl(coverId?: string): Promise<void> {
    await purgeCache(getCoversEndpoint, getCoverEndpoint, coverId)
}

export async function fetchMangaCoverUrls(mangas: Array<Manga>): Promise<Array<string | undefined>> {
    const coverIds = mapRelationships(mangas, AttributeType.CoverArt, false).map((rel) => rel.id)
    const covers = await fetchResourcesByIds<AttributeType.CoverArt>(getCoversEndpoint, getCoverEndpoint, coverIds)
    const coverUrls: Array<string | undefined> = []

    for (const manga of mangas) {
        const mangaId = manga.id
        const cover = covers.find((cover) => findRelationship(cover, AttributeType.Manga)?.id === mangaId)
        coverUrls.push(getCoverUrl(mangaId, cover?.attributes))
    }

    return coverUrls
}
