import { MD_API_CACHE_KEY, PROXY_API_URL, SEASONAL_LIST_ID } from '@/common/Constants'
import { Manga, MangaQuery } from './schemas/Manga'
import { purgeCachedCoverUrl } from './cover'
import { purgeCachedMangaRead } from './mangaRead'
import { purgeCachedFollowStatus } from './follows'
import { purgeCachedScanlationGroup } from './scanlationGroup'
import { purgeCache } from './utils/purgeCache'
import { purgeCachedMangaFeed } from './mangaFeed'
import { fetchPaginatedResources } from './utils/fetchPaginatedResources'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { AttributeType, findAllRelationships, PaginatedResult } from './schemas/Result'
import { fetchListsByIds } from './customList'
import { purgeCachedAuthor } from './author'
import { purgeCachedMangaStatistic } from './statistic'

function getMangasEndpoint() {
    return `${PROXY_API_URL}/manga`
}

function getMangaEndpoint(mangaId: string) {
    return `${PROXY_API_URL}/manga/${mangaId}`
}

export async function purgeCachedMangas(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    const matchedResponses = (await cache.matchAll(getMangasEndpoint(), { ignoreSearch: true })).filter((res) => !res.url.includes('ids[]='))
    await Promise.all(matchedResponses.map((response) => cache.delete(response.url)))
}

export async function purgeCachedManga(mangaId: string, coverId: string | undefined, groupIds: Array<string>, authorIds: Array<string>): Promise<void> {
    await Promise.all([
        purgeCache(getMangasEndpoint, getMangaEndpoint, mangaId),
        purgeCachedMangaFeed(mangaId),
        purgeCachedMangaStatistic(mangaId),

        purgeCachedCoverUrl(coverId),
        purgeCachedFollowStatus(mangaId),
        purgeCachedMangaRead(mangaId),

        ...groupIds.map((groupId) => purgeCachedScanlationGroup(groupId)),
        ...authorIds.map((authorId) => purgeCachedAuthor(authorId)),
    ])
}

export const FETCH_MANGA_BASE_QUERY: MangaQuery = {
    includes: [
        AttributeType.CoverArt,
        AttributeType.Author,
        AttributeType.Artist,
        AttributeType.Manga,
    ],
}

export async function fetchMangasByIds(mangaIds: Array<string>, queryOverrides?: MangaQuery): Promise<Array<Manga>> {
    const query: MangaQuery = {
        ...FETCH_MANGA_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchResourcesByIds<AttributeType.Manga>(getMangasEndpoint, getMangaEndpoint, mangaIds, query)
}

export async function fetchMangas(queryOverrides?: MangaQuery): Promise<PaginatedResult<AttributeType.Manga>> {
    const query: MangaQuery = {
        ...FETCH_MANGA_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchPaginatedResources(getMangasEndpoint, query)
}

export async function fetchSeasonalMangas(): Promise<Array<Manga>> {
    const list = await fetchListsByIds([SEASONAL_LIST_ID])
    const mangaIds = findAllRelationships(list[0], AttributeType.Manga).map((rel) => rel.id)
    return await fetchMangasByIds(mangaIds)
}
