import { MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { Chapter, ChapterQuery } from './schemas/Chapter'
import { fetchPaginatedResources } from './utils/fetchPaginatedResources'
import { purgeCache } from './utils/purgeCache'
import { fetchResourcesByIds } from './utils/fetchResourcesByIds'
import { Language } from './schemas/Language'
import { AttributeType, PaginatedResult } from './schemas/Result'

function getChaptersEndpoint() {
    return `${PROXY_API_URL}/chapter`
}

function getChapterEndpoint(chapterId: string) {
    return `${PROXY_API_URL}/chapter/${chapterId}`
}

export async function purgeCachedChaptersFeed(): Promise<void> {
    if (!DEFINE.HAS_SERVICE_WORKER) {
        return
    }

    const cache = await caches.open(MD_API_CACHE_KEY)
    const matchedResponses = (await cache.matchAll(getChaptersEndpoint(), { ignoreSearch: true })).filter((res) => !res.url.includes('ids[]='))
    await Promise.all(matchedResponses.map((response) => cache.delete(response.url)))
}

export async function purgeCachedChapter(chapterId?: string): Promise<void> {
    await purgeCache(getChaptersEndpoint, getChapterEndpoint, chapterId)
}

export const FETCH_CHAPTERS_BASE_QUERY: ChapterQuery = {
    includeFutureUpdates: '0',
    translatedLanguage: [
        Language.English,
    ],
    includes: [
        AttributeType.ScanlationGroup,
    ],
}

export async function fetchChaptersByIds(chapterIds: Array<string>, queryOverrides?: ChapterQuery): Promise<Array<Chapter>> {
    const query: ChapterQuery = {
        ...FETCH_CHAPTERS_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchResourcesByIds<AttributeType.Chapter>(getChaptersEndpoint, getChapterEndpoint, chapterIds, query)
}

export async function fetchChapters(queryOverrides?: ChapterQuery): Promise<PaginatedResult<AttributeType.Chapter>> {
    const query: ChapterQuery = {
        ...FETCH_CHAPTERS_BASE_QUERY,
        ...queryOverrides,
    }

    return await fetchPaginatedResources(getChaptersEndpoint, query)
}
