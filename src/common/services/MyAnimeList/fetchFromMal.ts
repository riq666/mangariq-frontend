import { CORS_PROXY_TARGET_ORIGIN_HEADER, MAL_API, MAL_CLIENT_ID, PING_TIMEOUT, PROXY_API_URL } from '@/common/Constants'
import axios from 'axios'
import { MalMangaResponse } from './MalMangaResponse'
import queryString from 'query-string'

export async function fetchFromMal<T extends keyof MalMangaResponse>(mangaId: string, fields: Array<T>): Promise<Pick<MalMangaResponse, T> | undefined> {
    const timeout = axios.CancelToken.source()
    const timeoutId = setTimeout(() => timeout.cancel(`Request timeout: ${origin}`), PING_TIMEOUT)
    let data: Pick<MalMangaResponse, T> | undefined

    try {
        const url = queryString.stringifyUrl({
            url: `${PROXY_API_URL}/manga/${mangaId}`,
            query: {
                fields,
            },
        }, {
            arrayFormat: 'comma',
        })

        const res = await axios.get<Pick<MalMangaResponse, T>>(url, {
            cancelToken: timeout.token,
            headers: {
                'X-MAL-CLIENT-ID': MAL_CLIENT_ID,
                [CORS_PROXY_TARGET_ORIGIN_HEADER]: MAL_API,
            },
        })
        data = res.data
    } catch (err) {
        console.warn(err)
    } finally {
        clearTimeout(timeoutId)
    }

    return data
}
