interface Picture {
    large: string | null
    medium: string
}

interface AltTitles {
    synonyms: Array<string> | null
    en: string | null
    ja: string | null
}

interface Genre {
    id: number
    name: string
}

interface Person {
    node: {
        id: number
        first_name: string
        last_name: string
    }
    role: string
}

interface Magazine {
    node: {
        id: number
        name: string
    }
    role: string
}

const enum NsfwStatus {
    Safe = 'white',
    Maybe = 'grey',
    NotSafe = 'black',
}

const enum MangaMediaType {
    Unknown = 'unknown',
    Manga = 'manga',
    Novel = 'novel',
    OneShot = 'one_shot',
    Doujinshi = 'doujinshi',
    Manhwa = 'manhwa',
    Manhua = 'manhua',
    Oel = 'oel',
}

const enum MangaPublishStatus {
    Finished = 'finished',
    CurrentlyPublishing = 'currently_publishing',
    NotYetPublished = 'not_yet_published',
}

const enum RelationType {
    Sequel = 'sequel',
    Prequel = 'prequel',
    AlternativeSetting = 'alternative_setting',
    AlternativeVersion = 'alternative_version',
    SideStory = 'side_story',
    ParentStory = 'parent_story',
    Summary = 'summary',
    FullStory = 'full_story',
}

interface MalItem {
    id: number

    title: string
    alternative_titles: AltTitles | null

    main_picture: Picture | null
    pictures: Array<Picture>

    start_date: string | null
    end_date: string | null
    synopsis: string | null
    background: string | null // Meta info about series (e.g. awards)

    mean: number | null
    rank: number | null
    popularity: number | null

    num_list_users: number
    num_scoring_users: number
    nsfw: NsfwStatus | null
    genres: Array<Genre>

    created_at: string
    updated_at: string
}

export type MalMangaResponse = MalItem & {
    media_type: MangaMediaType
    status: MangaPublishStatus
    num_volumes: number
    num_chapters: number
    authors: Array<Person>

    related_anime: Array<{
        node: MalItem
        relation_type: RelationType
        relation_type_formatted: string
    }>
    related_manga: Array<{
        node: MalItem
        relation_type: RelationType
        relation_type_formatted: string
    }>
    recommendations: Array<{
        node: MalItem
        num_recommendations: number
    }>
    serialization: Array<Magazine>
}
