import { JIKAN_API, PING_TIMEOUT } from '@/common/Constants'
import axios from 'axios'
import { JikanMangaResponse } from './JikanMangaResponse'

export async function fetchFromJikan(mangaId: string): Promise<JikanMangaResponse | undefined> {
    const timeout = axios.CancelToken.source()
    const timeoutId = setTimeout(() => timeout.cancel(`Request timeout: ${origin}`), PING_TIMEOUT)
    let data: JikanMangaResponse | undefined

    try {
        const res = await axios.get<JikanMangaResponse>(`${JIKAN_API}/manga/${mangaId}`, { cancelToken: timeout.token })
        data = res.data
    } catch (err) {
        console.warn(err)
    } finally {
        clearTimeout(timeoutId)
    }

    return data
}
