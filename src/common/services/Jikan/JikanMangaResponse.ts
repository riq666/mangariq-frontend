export interface JikanMangaResponse {
    score: number | null
    rank: number | null
    popularity: number | null
    members: number | null
}
