export const APP_NAME = 'SimpleDex'
export const APP_DESC = 'SimpleDex is a high contrast readonly frontend for browsing MangaDex v5'
export const APP_THEME_COLOR = '#ff6740'

export const SENTRY_DSN = 'https://998143c2f90148cdb0d050e5a4ef3120@o504161.ingest.sentry.io/5906208'

export const JIKAN_API = 'https://api.jikan.moe/v3'
export const MAL_API = 'https://api.myanimelist.net/v2'
export const MAL_CLIENT_ID = '5562ce5696c311a8e66feaa72535e09d'

export const MD_API_URL = 'https://api.mangadex.org'
export const MD_NETWORK_API_URL = 'https://api.mangadex.network'

export const CORS_PROXY_TARGET_ORIGIN_HEADER = 'X-TARGET-ORIGIN'
export const CORS_PROXY_URL = 'https://simpledex.trinovantes.workers.dev'

export const FRONTEND_URL = DEFINE.IS_DEV
    ? 'http://localhost:8080'
    : 'https://simpledex.pages.dev'

export const PROXY_API_URL = DEFINE.IS_DEV
    ? 'http://localhost:3000'
    : CORS_PROXY_URL

export const MD_API_CACHE_KEY = 'mdapi'

export const PING_TIMEOUT = 5000 // ms
export const ACCESS_TOKEN_LIFESPAN = 15 * 60 // 15 min
export const QUERY_LIMIT = 24
export const MAX_RESULTS = 10000
export const MAX_IDS = 100

export const SEASONAL_LISTS = [
    'ff210dec-862b-4c17-8608-0e7f97c70488', // Winter 2022
    'd434f5f1-ff90-4fa5-be7c-2c070da79120', // Fall 2021
    'a153b4e6-1fcc-4f45-a990-f37f989c0d74', // Summer 2021
]

export const SEASONAL_LIST_ID = SEASONAL_LISTS[0]
