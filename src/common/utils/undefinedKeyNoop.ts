export function mapGet<K, V>(map: Map<K, V>, key: K | undefined): V | undefined {
    if (key === undefined) {
        return undefined
    }

    return map.get(key)
}

export function mapGetAll<K, V>(map: Map<K, V>, keys: Array<K>): Array<V> {
    const values: Array<V> = []

    for (const key of keys) {
        const val = map.get(key)
        if (val === undefined) {
            continue
        }

        values.push(val)
    }

    return values
}

export function mapDelete<K, V>(map: Map<K, V>, key: K | undefined): boolean {
    if (key === undefined) {
        return false
    }

    return map.delete(key)
}

export function setAdd<T>(set: Set<T>, val: T | undefined): void {
    if (val === undefined) {
        return
    }

    set.add(val)
}

export function arrayPush<T>(array: Array<T>, val: T | undefined): number {
    if (val === undefined) {
        return array.length
    }

    return array.push(val)
}
