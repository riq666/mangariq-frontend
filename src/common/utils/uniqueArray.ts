export function uniqueArray<T>(array: Array<T>): Array<T> {
    const set = new Set(array)
    return [...set]
}
