export function pascalCaseToSpaced(s: string): string {
    return s.replace(/([A-Z])/g, (c) => ` ${c}`).trim()
}
