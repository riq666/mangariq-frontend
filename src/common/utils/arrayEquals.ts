import _ from 'lodash'

export function arrayEquals<T>(a: Array<T>, b: Array<T>): boolean {
    return _.isEqual([...a].sort(), [...b].sort())
}
