export const EPSILON = 2e-11

export function floatEq(a: number, b: number): boolean {
    return Math.abs(a - b) <= EPSILON
}

export function floatGt(a: number, b: number): boolean {
    return !floatEq(a, b) && a > b
}
