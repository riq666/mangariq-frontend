// ----------------------------------------------------------------------------
// Enable service worker definitions
/// <reference lib="WebWorker" />
declare const self: ServiceWorkerGlobalScope & { __WB_DISABLE_DEV_LOGS: boolean }
// ----------------------------------------------------------------------------

import { setupUpdater } from './setupUpdater'
import { setupCache } from './setupCache'

// Disable logging
self.__WB_DISABLE_DEV_LOGS = true

setupUpdater(self)
setupCache()
