import { JIKAN_API, MD_API_CACHE_KEY, PROXY_API_URL } from '@/common/Constants'
import { imageCache, staticResourceCache, googleFontsCache } from 'workbox-recipes'
import { registerRoute } from 'workbox-routing'
import { CacheFirst } from 'workbox-strategies'
import { CacheableResponsePlugin } from 'workbox-cacheable-response'
import { ExpirationPlugin } from 'workbox-expiration'

export function setupCache(): void {
    // Cache static assets (css/js)
    if (!DEFINE.IS_DEV) {
        staticResourceCache()
    }

    // Cache cover images
    imageCache({
        maxEntries: 1000,
        matchCallback: ({ url }) => {
            if (url.origin !== 'https://uploads.mangadex.org') {
                return false
            }

            if (!url.pathname.startsWith('/covers')) {
                return false
            }

            return true
        },
    })

    // Cache fonts
    googleFontsCache()

    // Cache Jikan for 30 days
    cacheRoute(30 * 24, ({ url }) => url.origin.startsWith(JIKAN_API))

    // Cache feeds for 12 hours
    cacheRoute(1, ({ url }) => {
        if (doNotCacheUrl(url)) {
            return false
        }

        if (url.pathname.endsWith('/feed')) {
            return true
        }

        if (url.pathname === '/manga' && !url.search.includes('ids[]=')) {
            return true
        }

        if (url.pathname === '/chapter' && !url.search.includes('ids[]=')) {
            return true
        }

        return false
    })

    // Cache static data for 30 days
    cacheRoute(30 * 24, ({ url }) => {
        if (doNotCacheUrl(url)) {
            return false
        }

        if (/^\/(manga|cover|group|author)\/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})$/.test(url.pathname)) {
            return true
        }

        return false
    })

    // Cache everything else for 1 week
    cacheRoute(7 * 24, ({ url }) => {
        if (doNotCacheUrl(url)) {
            return false
        }

        return true
    })
}

function doNotCacheUrl(url: URL): boolean {
    if (!url.origin.startsWith(PROXY_API_URL)) {
        return true
    }

    if (url.pathname.startsWith('/ping')) {
        return true
    }

    if (url.pathname.startsWith('/auth/')) {
        return true
    }

    if (url.pathname.startsWith('/at-home')) {
        return true
    }

    if (url.pathname === '/manga/random') {
        return true
    }

    return false
}

function cacheRoute(hours: number, routeMatcher: Parameters<typeof registerRoute>[0]) {
    registerRoute(
        routeMatcher,
        new CacheFirst({
            cacheName: MD_API_CACHE_KEY,
            plugins: [
                new CacheableResponsePlugin({
                    statuses: [0, 200],
                }),
                new ExpirationPlugin({
                    maxEntries: 10000,
                    maxAgeSeconds: 3600 * hours,
                }),
            ],
        }),
    )
}
