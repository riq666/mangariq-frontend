export function setupUpdater(self: ServiceWorkerGlobalScope): void {
    self.addEventListener('message', (event) => {
        if (event.data === null || event.data === undefined) {
            return
        }

        if (!(typeof event.data === 'object')) {
            return
        }

        const data = event.data as { type?: string }
        if (data.type !== 'SKIP_WAITING') {
            return
        }

        // Skip waiting stage of lifecycle and activate itself
        void self.skipWaiting()
    })
}
