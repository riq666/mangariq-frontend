import { HydrationKey } from '@/web/client/store/Hydration'

declare global {
    const DEFINE: {
        IS_DEV: boolean
        IS_SSR: boolean
        GIT_HASH: string
        HAS_SERVICE_WORKER: boolean
    }

    interface Window {
        [HydrationKey.USER]?: string
        [HydrationKey.FOLLOWS_FILTER]?: string
        [HydrationKey.SEARCH_FILTER]?: string
    }
}

export {}
