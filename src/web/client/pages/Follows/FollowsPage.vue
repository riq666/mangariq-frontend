<script lang="ts">
import { computed, defineAsyncComponent, defineComponent, onMounted, ref, watch } from 'vue'
import { useMangaStore } from '@/web/client/store/Manga'
import { Manga, MangaStatus, ReadingStatus, READING_STATUS_OPTIONS, MANGA_STATUS_OPTIONS } from '@/common/services/MangaDex/schemas/Manga'
import { createPageHeadOptions } from '@/web/client/utils/PageHeadOptions'
import { useMeta } from 'vue-meta'
import { useUserStore } from '@/web/client/store/User'
import { purgeCachedUserFollows } from '@/common/services/MangaDex/follows'
import { createLongTaskRunner } from '@/web/client/utils/createLongTaskRunner'
import { usePagination } from '@/web/client/utils/usePagination'
import { useFollowsFilterStore } from '@/web/client/store/FollowsFilter'
import { QUERY_LIMIT } from '@/common/Constants'
import { LocalizedString } from '@/common/services/MangaDex/schemas/Language'

export default defineComponent({
    components: {
        MangaTile: defineAsyncComponent(() => import('@/web/client/components/Manga/MangaTile.vue')),
        MangaStatus: defineAsyncComponent(() => import('@/web/client/components/Manga/MangaStatus.vue')),
        ChangeReadingStatus: defineAsyncComponent(() => import('@/web/client/components/Manga/ChangeReadingStatus.vue')),
    },

    props: {
        page: {
            type: Number,
            required: true,
        },
    },

    setup(props) {
        const title = 'Follows'
        useMeta(createPageHeadOptions({
            title,
        }))

        const pageProp = computed(() => props.page)
        const { maxPages, currentPage, updateMaxPage } = usePagination(pageProp)

        const followsFilterStore = useFollowsFilterStore()
        const mangaStatusFilter = computed<MangaStatus | null>({
            get: () => {
                return followsFilterStore.mangaStatus
            },
            set: (val) => {
                followsFilterStore.mangaStatus = val
            },
        })
        const readingStatusFilter = computed<ReadingStatus | null>({
            get: () => {
                return followsFilterStore.readingStatus
            },
            set: (val) => {
                followsFilterStore.readingStatus = val
            },
        })
        const titleFilter = computed<string | null>({
            get: () => {
                return followsFilterStore.title
            },
            set: (val) => {
                followsFilterStore.title = val
            },
        })

        const userStore = useUserStore()
        const mangaStore = useMangaStore()
        const followedMangas = ref<Array<Manga>>([])
        const loadMangas = createLongTaskRunner(async() => {
            const accessToken = await userStore.getValidToken()
            followedMangas.value = await mangaStore.fetchUserFollowedMangas(accessToken)
        })
        const filteredFollowedMangas = computed<Array<Manga>>(() => followedMangas.value.filter((manga) => {
            if (readingStatusFilter.value && readingStatusFilter.value !== mangaStore.mangaReadingStatus.get(manga.id)) {
                return false
            }

            if (mangaStatusFilter.value && mangaStatusFilter.value !== manga.attributes.status) {
                return false
            }

            if (titleFilter.value && !substrExists(manga.attributes.title, titleFilter.value)) {
                return false
            }

            return true
        }))

        const mangasOnCurrentPage = ref<Array<Manga>>([])
        watch([filteredFollowedMangas, pageProp], () => {
            updateMaxPage(0)
            const start = QUERY_LIMIT * (props.page - 1)
            const end = QUERY_LIMIT * (props.page)
            mangasOnCurrentPage.value = filteredFollowedMangas.value.slice(start, end)
            updateMaxPage(filteredFollowedMangas.value.length)
        })

        onMounted(loadMangas)

        const purgeCacheAndReload = async() => {
            await purgeCachedUserFollows()
            await loadMangas()
        }

        return {
            title,
            maxPages,
            currentPage,

            mangaStatusFilter,
            MANGA_STATUS_OPTIONS,

            readingStatusFilter,
            READING_STATUS_OPTIONS,

            titleFilter,

            mangasOnCurrentPage,
            purgeCacheAndReload,
        }
    },
})

function substrExists(haystack: LocalizedString, needle: string): boolean {
    const substr = needle.toLocaleLowerCase()

    for (const str of Object.values(haystack)) {
        if (str.toLocaleLowerCase().indexOf(substr) >= 0) {
            return true
        }
    }

    return false
}
</script>

<template>
    <article class="follows-page container">
        <h1>
            {{ title }}

            <q-select
                v-model="mangaStatusFilter"
                :options="MANGA_STATUS_OPTIONS"
                label="Filter by Manga Status"
                color="info"
                clearable
                outlined
                emit-value
                map-options
            />
            <q-select
                v-model="readingStatusFilter"
                :options="READING_STATUS_OPTIONS"
                label="Filter by Reading Status"
                color="info"
                clearable
                outlined
                emit-value
                map-options
            />
            <q-input
                v-model="titleFilter"
                label="Filter by Manga Title"
                color="info"
                outlined
                clearable
                debounce="500"
            />

            <q-btn
                icon="delete"
                label="Clear Cache"
                title="Clear cached data and reload information for this page"
                outline
                no-caps
                @click="purgeCacheAndReload"
            />
        </h1>

        <section>
            <MangaTile
                v-for="manga of mangasOnCurrentPage"
                :key="manga.id"
                :manga-id="manga.id"
                :height="400"
            >
                <template #default="{ mangaId }">
                    <MangaStatus
                        :manga-id="mangaId"
                        :is-small="true"
                    />

                    <ChangeReadingStatus
                        :manga-id="mangaId"
                        dark
                    />
                </template>
            </MangaTile>
        </section>

        <q-pagination
            v-show="maxPages > 1"
            v-model="currentPage"
            :max="maxPages"
            :max-pages="5"
            :ellipses="false"
            direction-links
            boundary-links
            color="dark"
        />
    </article>
</template>

<style lang="scss">
.follows-page{
    h1{
        .q-select,
        .q-input{
            margin: 0;
            flex: 1;
        }
    }

    section{
        display: grid;
        gap: $padding * 2;

        @media (min-width: $breakpoint-sm-min) {
            grid-template-columns: repeat(2, 1fr);
        }

        @media (min-width: $breakpoint-md-min) {
            grid-template-columns: repeat(3, 1fr);
        }

        @media (min-width: $breakpoint-lg-min) {
            grid-template-columns: repeat(4, 1fr);
        }

        @media (min-width: $breakpoint-xl-min) {
            grid-template-columns: repeat(6, 1fr);
        }

        .manga-tile{
            .q-select{
                flex: 1;
                margin: 0;
            }
        }
    }
}
</style>
