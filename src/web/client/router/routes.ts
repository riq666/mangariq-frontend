import { RouteRecordRaw } from 'vue-router'
import { parsePageParam, parsePageQuery } from './routeParsers'

export enum RouteName {
    Home = 'Home',
    Search = 'Search',
    Login = 'Login',
    Account = 'Account',
    Updates = 'Updates',
    Follows = 'Follows',
    Manga = 'Manga',
    Reader = 'Reader',
    Error404 = 'Error404',
}

export const NON_AUTHENTICATED_PAGES: Array<RouteName> = [
    RouteName.Login,
]

export const AUTHENTICATED_PAGES: Array<RouteName> = [
    RouteName.Updates,
    RouteName.Follows,
]

export const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: () => import('@/web/client/layouts/MainLayout.vue'),
        children: [
            {
                name: RouteName.Home,
                path: '',
                component: () => import('@/web/client/pages/Home/HomePage.vue'),
                props: (route) => ({
                    page: parsePageQuery(route),
                }),
            },
            {
                name: RouteName.Search,
                path: 'search',
                component: () => import('@/web/client/pages/Search/SearchPage.vue'),
                props: (route) => ({
                    page: parsePageQuery(route),
                }),
            },
            {
                name: RouteName.Login,
                path: 'login',
                component: () => import('@/web/client/pages/Login/LoginPage.vue'),
            },
            {
                name: RouteName.Account,
                path: 'account',
                component: () => import('@/web/client/pages/Account/AccountPage.vue'),
            },
            {
                name: RouteName.Updates,
                path: 'updates',
                alias: 'titles/feed',
                component: () => import('@/web/client/pages/Updates/UpdatesPage.vue'),
                props: (route) => ({
                    page: parsePageQuery(route),
                }),
            },
            {
                name: RouteName.Follows,
                path: 'follows',
                alias: 'titles/follows',
                component: () => import('@/web/client/pages/Follows/FollowsPage.vue'),
                props: (route) => ({
                    page: parsePageQuery(route),
                }),
            },
            {
                path: 'manga/new',
                component: () => import('@/web/client/pages/Manga/NewMangaPage.vue'),
                props: true,
            },
            {
                name: RouteName.Manga,
                path: 'manga/:mangaId/:slug?',
                alias: 'title/:mangaId/:slug?',
                component: () => import('@/web/client/pages/Manga/MangaPage.vue'),
                props: true,
            },
            {
                name: RouteName.Reader,
                path: 'chapter/:chapterId/:page?',
                component: () => import('@/web/client/pages/Reader/ReaderPage.vue'),
                props: (route) => ({
                    ...route.params,
                    page: parsePageParam(route),
                }),
            },
            {
                name: RouteName.Error404,
                path: '404',
                component: () => import('@/web/client/pages/Error/404Page.vue'),
            },
        ],
    },
    {
        path: '/:pathMatch(.*)*',
        redirect: {
            name: RouteName.Error404,
        },
    },
]
