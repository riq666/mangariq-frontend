import { RouteLocationNormalized } from 'vue-router'

export function getRouteArg(route: RouteLocationNormalized, component: 'params' | 'query', key: string): string {
    const val = route[component][key]
    const valString = Array.isArray(val)
        ? val[0]
        : val

    return valString ?? ''
}

export function parsePageQuery(route: RouteLocationNormalized): number {
    const pageString = getRouteArg(route, 'query', 'page')
    const page = parseInt(pageString)

    if (isNaN(page)) {
        return 1
    }

    return page
}

export function parsePageParam(route: RouteLocationNormalized): number {
    const pageString = getRouteArg(route, 'params', 'page')
    const page = parseInt(pageString)

    if (isNaN(page)) {
        return 1
    }

    return page
}
