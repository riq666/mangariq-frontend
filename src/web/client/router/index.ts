import { createMemoryHistory, createRouter, createWebHistory, Router, RouteRecordName } from 'vue-router'
import { AppContext } from '@/web/AppContext'
import { AUTHENTICATED_PAGES, NON_AUTHENTICATED_PAGES, RouteName, routes } from './routes'
import { computed, watch } from 'vue'
import { createPinia } from 'pinia'
import { useUserStore } from '../store/User'

// ----------------------------------------------------------------------------
// Router
// ----------------------------------------------------------------------------

export async function createAppRouter(pinia: ReturnType<typeof createPinia>, ssrContext?: AppContext): Promise<Router> {
    const router = createRouter({
        history: ssrContext !== undefined
            ? createMemoryHistory()
            : createWebHistory(),

        routes,

        scrollBehavior(to) {
            if (to.hash) {
                return { el: to.hash }
            }

            // Do not scroll when navigating to a ReaderPage route since it will manully scroll to its initial page offset
            if (to.name === RouteName.Reader) {
                return {}
            }

            // Always scroll to top of other pages since there's too many edge cases that break where we want to scroll and where we actually end up
            return { top: 0 }
        },
    })

    if (ssrContext?.url) {
        await router.push(ssrContext.url)
    }

    const userStore = useUserStore(pinia)
    const isLoggedIn = computed(() => userStore.isLoggedIn)
    const canVisitRoute = (currentRouteName?: RouteRecordName | null) => {
        if (!isLoggedIn.value && AUTHENTICATED_PAGES.includes(currentRouteName?.toString() as RouteName)) {
            return false
        }
        if (isLoggedIn.value && NON_AUTHENTICATED_PAGES.includes(currentRouteName?.toString() as RouteName)) {
            return false
        }

        return true
    }

    watch(isLoggedIn, async() => {
        if (canVisitRoute(router.currentRoute.value.name)) {
            return
        }

        console.warn(`Redirecting from ${router.currentRoute.value.fullPath} because isLoggedIn:${isLoggedIn.value}`)
        await router.push({ name: RouteName.Home })
    })

    router.beforeEach((to, from, next) => {
        if (canVisitRoute(to.name)) {
            next()
        } else {
            console.warn(`Cannot navigate to ${to.fullPath} because isLoggedIn:${isLoggedIn.value}`)
            next({ name: RouteName.Home })
        }
    })

    return router
}
