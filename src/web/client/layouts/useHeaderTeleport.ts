import { inject, provide, ref } from 'vue'

export const HEADER_LEFT_REF_KEY = 'HEADER_LEFT_REF_KEY'
export const HEADER_RIGHT_REF_KEY = 'HEADER_RIGHT_REF_KEY'

export function provideHeaderRefs() {
    const headerLeftRef = ref<HTMLDivElement | null>(null)
    const headerRightRef = ref<HTMLDivElement | null>(null)

    provide(HEADER_LEFT_REF_KEY, headerLeftRef)
    provide(HEADER_RIGHT_REF_KEY, headerRightRef)
}

export function injectHeaderRefs() {
    const headerLeftRef = inject(HEADER_LEFT_REF_KEY) as HTMLDivElement | null
    const headerRightRef = inject(HEADER_RIGHT_REF_KEY) as HTMLDivElement | null

    return {
        headerLeftRef,
        headerRightRef,
    }
}
