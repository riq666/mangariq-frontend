import { ErrorResult } from '@/common/services/MangaDex/schemas/Result'
import axios from 'axios'
import { useQuasar } from 'quasar'
import { Ref } from 'vue'

export function createLongTaskRunner(longTask: () => Promise<void>, showLoading?: Ref<boolean>): () => Promise<void> {
    const $q = useQuasar()
    const notify = (message: string, info: string) => {
        const dismiss = $q.notify({
            type: 'negative',
            textColor: 'white',
            icon: 'error',
            timeout: 0,
            html: true,
            message: `
                <h3 class="no-margin">${message}</h3>
                <pre class="q-mt-sm q-mb-none gt-sm">${info}</pre>
            `,
            actions: [
                {
                    icon: 'close',
                    round: true,
                    textColor: 'white',
                    handler: () => {
                        dismiss?.()
                    },
                },
            ],
        })
    }

    return async() => {
        const show = showLoading?.value ?? true

        // Since Quasar loading modifies the scroll behavior of <body>
        // Do not use it when we also need to call window.scroll after the longTask
        show && $q.loading.show()

        try {
            await longTask()
        } catch (err) {
            if (axios.isAxiosError(err)) {
                console.warn('Request Failed', err.response?.data)
            } else {
                console.warn(err)
            }

            if (!(err instanceof Error)) {
                throw err
            }

            notify(err.message, err.stack ?? err.name)

            if (axios.isAxiosError(err)) {
                const status = err.response?.status ?? 0
                if (status >= 400 && status < 500) {
                    const errors = (err.response?.data as ErrorResult | undefined)?.errors ?? []
                    for (const error of errors) {
                        const message = `${error.detail}`
                        const info = JSON.stringify(error, null, 4)
                        notify(message, info)
                    }
                }
            }
        }

        show && $q.loading.hide()
    }
}
