import { onBeforeMount, onBeforeUnmount, ref } from 'vue'

export function useWindowSize() {
    const w = ref(0)
    const h = ref(0)

    const updateWindowSize = () => {
        w.value = window.outerWidth
        h.value = window.outerHeight
    }

    updateWindowSize()

    onBeforeMount(() => {
        window.addEventListener('resize', updateWindowSize)
    })

    onBeforeUnmount(() => {
        window.removeEventListener('resize', updateWindowSize)
    })

    return {
        w,
        h,
    }
}
