export function downloadLink(src: string, dest: string): void {
    const a = document.createElement('a')
    a.href = src
    a.download = dest
    a.click()
    a.remove()
}
