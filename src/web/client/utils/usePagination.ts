import { MAX_RESULTS, QUERY_LIMIT } from '@/common/Constants'
import { ref, Ref, watch } from 'vue'
import { useRouter } from 'vue-router'

interface Exports {
    maxPages: Ref<number>
    currentPage: Ref<number>

    updateMaxPage: (max: number) => void
}

export function usePagination(pageProp: Ref<number>, queryLimit = QUERY_LIMIT): Exports {
    const maxPages = ref(0)
    const updateMaxPage = (val: number) => {
        maxPages.value = Math.ceil(Math.min(val, MAX_RESULTS) / queryLimit)
    }

    const currentPage = ref(pageProp.value)
    watch(pageProp, (page) => {
        currentPage.value = page
    })

    const router = useRouter()
    watch(currentPage, async(newPage, oldPage) => {
        if (newPage === oldPage) {
            return
        }

        console.info(`Navigate to page:${newPage}`)
        await router.push({
            query: {
                ...router.currentRoute.value.query,
                page: newPage,
            },
        })
    })

    return {
        maxPages,
        currentPage,
        updateMaxPage,
    }
}
