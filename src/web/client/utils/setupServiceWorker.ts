import { APP_NAME } from '@/common/Constants'
import { useQuasar } from 'quasar'
import { Workbox } from 'workbox-window'

export async function setupServiceWorker(): Promise<void> {
    if (!('serviceWorker' in navigator)) {
        return
    }

    const $q = useQuasar()
    const wb = new Workbox('/serviceWorker.js')
    let dismiss: ReturnType<typeof $q.notify> | null = null

    wb.addEventListener('activated', (event) => {
        if (!event.isUpdate) {
            console.info('Service worker activated for the first time')
            return
        }

        // Even if the user has multiple tabs open ("clients"), there is only 1 service worker instance
        // When 1 tab allows the worker to update, all the tabs get the latest update
        // Therefore, all active clients need to be reloaded
        dismiss?.()
        window.location.reload()
    })

    wb.addEventListener('waiting', () => {
        dismiss = $q.notify({
            message: `An update is available for ${APP_NAME}`,
            icon: 'get_app',
            color: 'dark',
            timeout: 0,
            actions: [
                {
                    label: 'Dismiss',
                    textColor: 'grey',
                    noCaps: true,
                    handler: () => {
                        dismiss?.()
                    },
                },
                {
                    label: 'Refresh',
                    textColor: 'primary',
                    noCaps: true,
                    handler: () => {
                        // Register listener now so it can be ready when the updated service worker to takes control (after it accepts the SKIP_WAITING message)
                        // Once it takes control, we force reload the page which allows the latest assets to be displayed
                        wb.addEventListener('controlling', () => {
                            window.location.reload()
                        })

                        wb.messageSkipWaiting()
                    },
                },
            ],
        })
    })

    await wb.register()
}
