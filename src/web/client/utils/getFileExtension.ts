export function getFileExtension(fileName: string): string | undefined {
    const matches = /.(\w+)$/.exec(fileName)
    if (!matches) {
        return undefined
    }

    return matches[1]
}
