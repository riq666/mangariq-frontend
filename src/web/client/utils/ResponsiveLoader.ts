export interface ResponsiveImage {
    src: string
    width?: number
    height?: number
    placeholder?: string
}

export function getImage(filename: string): ResponsiveImage {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    return require(`@/web/client/assets/img/${filename}`) as ResponsiveImage
}

export async function loadImage(filename: string): Promise<ResponsiveImage> {
    return import(`@/web/client/assets/img/${filename}`) as Promise<ResponsiveImage>
}
