export enum ReaderDirection {
    LeftToRight = 'ltr',
    RightToLeft = 'rtl',
}
