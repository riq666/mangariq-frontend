export enum PageMode {
    Single = 'single',
    TwoEven = 'two-even',
    TwoOdd = 'two-odd',
}
