<script lang="ts">
import { LANGUAGE_OPTIONS } from '@/common/services/MangaDex/schemas/Language'
import { MangaEditRequest, MANGA_STATUS_OPTIONS, CONTENT_RATING_OPTIONS, PUBLICATION_DEMOGRAPHIC_OPTIONS } from '@/common/services/MangaDex/schemas/Manga'
import { computed, defineAsyncComponent, defineComponent, onMounted, PropType, ref, watch } from 'vue'
import { useMangaStore } from '@/web/client/store/Manga'
import { capitalize } from 'lodash'
import { QForm, useQuasar } from 'quasar'

export default defineComponent({
    components: {
        FieldLocalizedString: defineAsyncComponent(() => import('./FieldLocalizedString.vue')),
        FieldLinks: defineAsyncComponent(() => import('./FieldLinks.vue')),
        FieldCreator: defineAsyncComponent(() => import('./FieldCreator.vue')),
    },

    props: {
        modelValue: {
            type: Object as PropType<MangaEditRequest>,
            required: true,
        },
    },

    emits: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        'update:modelValue': (mangaRequest: MangaEditRequest) => true,

        submit: () => true,
    },

    setup(props, { emit }) {
        const mangaStore = useMangaStore()

        onMounted(async() => await mangaStore.fetchTags())
        const tagGroups = computed(() => {
            const options = new Map<string, Array<{ label: string; value: string }>>()

            for (const [tagId, tag] of mangaStore.tag.entries()) {
                const enName = tag.attributes.name.en
                if (!enName) {
                    continue
                }

                const groupName = capitalize(tag.attributes.group)
                if (!options.has(groupName)) {
                    options.set(groupName, [])
                }

                options.get(groupName)?.push({
                    label: enName,
                    value: tagId,
                })
            }

            for (const tags of options.values()) {
                tags.sort((tagA, tagB) => tagA.label.localeCompare(tagB.label))
            }

            return options
        })

        const mangaRequest = ref<MangaEditRequest>(props.modelValue)
        watch(mangaRequest, (mangaRequest) => emit('update:modelValue', mangaRequest), { deep: true })

        const $q = useQuasar()
        const formRef = ref<QForm | null>(null)
        const onFormSubmit = async() => {
            // Run base validation rules
            const isValid = await formRef.value?.validate()
            if (!isValid) {
                return
            }

            // Check if at least 1 title exists
            if (Object.keys(mangaRequest.value.title).length === 0) {
                const dismiss = $q.notify({
                    type: 'negative',
                    textColor: 'white',
                    icon: 'error',
                    message: 'Manga must have title in at least 1 language',
                    actions: [
                        {
                            icon: 'close',
                            round: true,
                            textColor: 'white',
                            handler: () => {
                                dismiss?.()
                            },
                        },
                    ],
                })

                window.scrollTo({ top: 0 })
                return
            }

            emit('submit')
        }

        return {
            tagGroups,
            LANGUAGE_OPTIONS,
            MANGA_STATUS_OPTIONS,
            CONTENT_RATING_OPTIONS,
            PUBLICATION_DEMOGRAPHIC_OPTIONS,

            mangaRequest,
            formRef,
            onFormSubmit,
        }
    },
})
</script>

<template>
    <q-form
        ref="formRef"
        class="manga-editor"
        @submit="onFormSubmit"
    >
        <div>
            <h2>
                Manga Title
            </h2>
            <FieldLocalizedString
                v-model="mangaRequest.title"
                label="Manga Name"
                :is-unique-languages="true"
            />
        </div>

        <div class="grid-2">
            <div>
                <h2>
                    Authors
                </h2>
                <FieldCreator
                    v-model="mangaRequest.authors"
                    label="Authors"
                    hint="Select an existing author or press ENTER to create a new author"
                    icon="face"
                    new-value-mode="add"
                />
            </div>

            <div>
                <h2>
                    Artists
                </h2>
                <FieldCreator
                    v-model="mangaRequest.artists"
                    label="Artists"
                    hint="Select an existing artist or press ENTER to create a new artist"
                    icon="brush"
                    new-value-mode="add"
                />
            </div>
        </div>

        <h2>
            Additional Information
        </h2>

        <div class="grid-5">
            <q-select
                v-model="mangaRequest.originalLanguage"
                :options="LANGUAGE_OPTIONS"
                label="Original Language"
                clearable
                outlined
                emit-value
                map-options
            />

            <q-select
                v-model="mangaRequest.status"
                :options="MANGA_STATUS_OPTIONS"
                label="Manga Status"
                clearable
                outlined
                emit-value
                map-options
            />

            <q-select
                v-model="mangaRequest.contentRating"
                :options="CONTENT_RATING_OPTIONS"
                label="Content Rating"
                clearable
                outlined
                emit-value
                map-options
            />

            <q-select
                v-model="mangaRequest.publicationDemographic"
                :options="PUBLICATION_DEMOGRAPHIC_OPTIONS"
                label="Publication Demographic"
                clearable
                outlined
                emit-value
                map-options
            />

            <q-input
                v-model.number="mangaRequest.year"
                label="Year of Release"
                outlined
                clearable
                hide-bottom-space
                :rules="[
                    (val: string | number) => (val === null || (Number.isInteger(val) && val >= 1 && val <= 9999)) || 'Year must be between 1 and 9999'
                ]"
            />
        </div>

        <div>
            <h2>
                Description
            </h2>
            <FieldLocalizedString
                v-model="mangaRequest.description"
                label="Manga Description"
                :is-textarea="true"
                :is-unique-languages="true"
            />
        </div>

        <div>
            <h2>
                Links
            </h2>
            <FieldLinks
                v-model="mangaRequest.links"
            />
        </div>

        <div>
            <h2>
                Alternative Titles
            </h2>
            <FieldLocalizedString
                v-model="mangaRequest.altTitles"
                label="Alternative Title"
            />
        </div>

        <div v-if="tagGroups.size > 0">
            <h2>
                Tags
            </h2>

            <div class="grid-4 bordered-box">
                <div
                    v-for="[groupName, tagOptions] of tagGroups.entries()"
                    :key="groupName"
                >
                    <h3>{{ groupName }}</h3>
                    <q-option-group
                        v-model="mangaRequest.tags"
                        :options="tagOptions"
                        type="checkbox"
                    />
                </div>
            </div>
        </div>

        <slot
            v-if="$slots.bottom"
            name="bottom"
        />

        <div class="actions">
            <q-btn
                label="Create Manga"
                icon="publish"
                color="positive"
                no-caps
                unelevated
                type="submit"
            />
        </div>
    </q-form>
</template>

<style lang="scss">
.manga-editor{
    .bordered-box{
        border: $border;
        padding: $padding;
    }

    img.cover-art-preview{
        border: $padding solid black;
        display: block;
        margin-top: $padding * 2;
        max-width: 100%;
    }
}
</style>
