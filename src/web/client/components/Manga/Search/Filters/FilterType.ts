import { Language } from '@/common/services/MangaDex/schemas/Language'
import { MangaStatus, ContentRating, PublicationDemographic } from '@/common/services/MangaDex/schemas/Manga'

export interface MetaFilterMap {
    originalLanguage: Array<Language>
    status: Array<MangaStatus>
    contentRating: Array<ContentRating>
    publicationDemographic: Array<PublicationDemographic>
}

export enum TagFilterType {
    Include,
    Exclude,
}

export enum CreatorType {
    Author,
    Artist,
}
