<script lang="ts">
import { defineComponent } from 'vue'
import { setupServiceWorker } from '@/web/client/utils/setupServiceWorker'
import { useFollowsFilterSyncLocalStorage } from '@/web/client/store/FollowsFilter/useFollowsFilterSyncLocalStorage'
import { useSearchFilterSyncLocalStorage } from '@/web/client/store/SearchFilter/useSearchFilterSyncLocalStorage'
import { useClearMangaDataOnLogout } from '@/web/client/store/Manga/useClearMangaDataOnLogout'

export default defineComponent({
    async setup() {
        useClearMangaDataOnLogout()
        useFollowsFilterSyncLocalStorage()
        useSearchFilterSyncLocalStorage()

        await setupServiceWorker()
    },
})
</script>

<template>
    <metainfo />

    <q-ajax-bar />

    <router-view v-slot="{ Component }">
        <template v-if="Component">
            <suspense>
                <component :is="Component" />
            </suspense>
        </template>
    </router-view>
</template>
