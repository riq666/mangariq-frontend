import { Language } from '@/common/services/MangaDex/schemas/Language'
import { DEFAULT_CONTENT_RATING, DEFAULT_PUBLICATION_DEMOGRAPHIC, DEFAULT_MANGA_STATUS, DEFAULT_ORIGINAL_LANGUAGE, MangaQuery, DEFAULT_INCLUDE_TAG_MODE, DEFAULT_EXCLUDE_TAG_MODE, MangaStatus, ContentRating, PublicationDemographic } from '@/common/services/MangaDex/schemas/Manga'
import { SortOrder } from '@/common/services/MangaDex/schemas/SortOrder'
import { arrayEquals } from '@/common/utils/arrayEquals'
import { defineStore } from 'pinia'
import { LocationQuery } from 'vue-router'
import { useMangaStore } from '../Manga'
import { getQueryArray, getQueryString, getQueryValidatedArray, getQueryValidatedString } from './utils'

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export type MangaSortQuery = Pick<MangaQuery,
    'order[title]' |
    'order[year]' |
    'order[createdAt]' |
    'order[updatedAt]' |
    'order[latestUploadedChapter]' |
    'order[followedCount]' |
    'order[relevance]'
>

export const MANGA_SORT_QUERY_KEYS: Array<keyof MangaSortQuery> = [
    'order[title]',
    'order[year]',
    'order[createdAt]',
    'order[updatedAt]',
    'order[latestUploadedChapter]',
    'order[followedCount]',
    'order[relevance]',
]

export type SearchFilterState = Pick<Required<MangaQuery>,
    'title' |
    'authors' |
    'artists' |

    'originalLanguage' |
    'status' |
    'contentRating' |
    'publicationDemographic' |

    'includedTags' |
    'includedTagsMode' |
    'excludedTags' |
    'excludedTagsMode'
> & MangaSortQuery

function createDefaultSearchFilterState(): SearchFilterState {
    const defaultState: SearchFilterState = {
        title: '',

        authors: [],
        artists: [],

        originalLanguage: DEFAULT_ORIGINAL_LANGUAGE,
        status: DEFAULT_MANGA_STATUS,
        contentRating: DEFAULT_CONTENT_RATING,
        publicationDemographic: DEFAULT_PUBLICATION_DEMOGRAPHIC,

        includedTags: [],
        includedTagsMode: DEFAULT_INCLUDE_TAG_MODE,
        excludedTags: [],
        excludedTagsMode: DEFAULT_EXCLUDE_TAG_MODE,
    }

    for (const orderKey of MANGA_SORT_QUERY_KEYS) {
        defaultState[orderKey] = undefined
    }

    return defaultState
}

// ----------------------------------------------------------------------------
// Store
// ----------------------------------------------------------------------------

export const useSearchFilterStore = defineStore('SearchFilter', {
    state: createDefaultSearchFilterState,

    actions: {
        exportToQuery(): MangaQuery {
            const mangaStore = useMangaStore()
            const query: MangaQuery = {}

            if (this.title) {
                query.title = this.title
            }

            for (const key of MANGA_SORT_QUERY_KEYS) {
                if (this.$state[key]) {
                    query[key] = this.$state[key]
                }
            }

            if (this.authors.length > 0) {
                query.authors = this.authors
            }
            if (this.artists.length > 0) {
                query.artists = this.artists
            }

            if (this.originalLanguage.length > 0 && !arrayEquals(this.originalLanguage, DEFAULT_ORIGINAL_LANGUAGE)) {
                query.originalLanguage = this.originalLanguage
            }
            if (this.status.length > 0 && !arrayEquals(this.status, DEFAULT_MANGA_STATUS)) {
                query.status = this.status
            }
            if (this.contentRating.length > 0 && !arrayEquals(this.contentRating, DEFAULT_CONTENT_RATING)) {
                query.contentRating = this.contentRating
            }
            if (this.publicationDemographic.length > 0 && !arrayEquals(this.publicationDemographic, DEFAULT_PUBLICATION_DEMOGRAPHIC)) {
                query.publicationDemographic = this.publicationDemographic
            }

            const includedTags = this.includedTags.filter((tagId) => mangaStore.tag.has(tagId))
            if (includedTags.length > 0) {
                query.includedTags = includedTags
                query.includedTagsMode = this.includedTagsMode
            }

            const excludedTags = this.excludedTags.filter((tagId) => mangaStore.tag.has(tagId))
            if (excludedTags.length > 0) {
                query.excludedTags = excludedTags
                query.excludedTagsMode = this.excludedTagsMode
            }

            return query
        },

        importFromQuery(query: LocationQuery) {
            const title = getQueryString(query, 'title')
            if (title) {
                this.title = title
            }

            for (const key of MANGA_SORT_QUERY_KEYS) {
                const order = getQueryValidatedString(query, key, Object.values(SortOrder))
                if (order) {
                    this.$state[key] = order
                }
            }

            const authors = getQueryArray(query, 'authors')
            if (authors.length > 0) {
                this.authors = authors
            }
            const artists = getQueryArray(query, 'artists')
            if (artists.length > 0) {
                this.artists = artists
            }

            const originalLanguage = getQueryValidatedArray(query, 'originalLanguage', Object.values(Language))
            if (originalLanguage.length > 0 && !arrayEquals(originalLanguage, DEFAULT_ORIGINAL_LANGUAGE)) {
                this.originalLanguage = originalLanguage
            }
            const status = getQueryValidatedArray(query, 'status', Object.values(MangaStatus))
            if (status.length > 0 && !arrayEquals(status, DEFAULT_MANGA_STATUS)) {
                this.status = status
            }
            const contentRating = getQueryValidatedArray(query, 'contentRating', Object.values(ContentRating))
            if (contentRating.length > 0 && !arrayEquals(contentRating, DEFAULT_CONTENT_RATING)) {
                this.contentRating = contentRating
            }
            const publicationDemographic = getQueryValidatedArray(query, 'publicationDemographic', Object.values(PublicationDemographic))
            if (publicationDemographic.length > 0 && !arrayEquals(publicationDemographic, DEFAULT_PUBLICATION_DEMOGRAPHIC)) {
                this.publicationDemographic = publicationDemographic
            }

            const mangaStore = useMangaStore()
            const allTags = [...mangaStore.tag.keys()]
            const includedTags = getQueryValidatedArray(query, 'includedTags', allTags)
            if (includedTags.length > 0) {
                this.includedTags = includedTags
            }
        },
    },
})
