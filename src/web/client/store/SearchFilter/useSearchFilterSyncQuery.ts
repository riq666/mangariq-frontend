import { useRouter } from 'vue-router'
import { computed, onBeforeMount, watch } from 'vue'
import { useSearchFilterStore } from '.'

export function useSearchFilterSyncQuery(): void {
    const searchFilterStore = useSearchFilterStore()
    const router = useRouter()
    const routeQuery = computed(() => router.currentRoute.value.query)
    const initPage = router.currentRoute.value.name

    // Avoid infinite loop due to one handler causing changes that will trigger the other handler
    let isProcessing = false

    const exportQuery = async() => {
        if (isProcessing) {
            return
        }

        isProcessing = true
        const exportedQuery = searchFilterStore.exportToQuery()
        await router.push({
            query: {
                ...routeQuery.value,
                ...exportedQuery,
            },
        })
        isProcessing = false
    }

    const importQuery = () => {
        if (isProcessing) {
            return
        }

        isProcessing = true
        searchFilterStore.importFromQuery(routeQuery.value)
        isProcessing = false
    }

    // Whenever the url query changes, we import the query into the state
    // Skip import when we navigate to a different route
    watch(routeQuery, () => {
        if (router.currentRoute.value.name !== initPage) {
            return
        }

        importQuery()
    }, {
        deep: true,
    })

    // Whenever the state changes, we export the query into the url
    searchFilterStore.$subscribe(() => { void exportQuery() })

    // This runs once on page load: load url query (and filtering out invalid params) then save result back to url
    // Use onBeforeMount hook instead of onMounted so that this executes before children components' onMounted hooks (that may modify state)
    onBeforeMount(async() => {
        const hasInitQuery = Object.keys(routeQuery.value).length > 0
        if (hasInitQuery) {
            isProcessing = true
            searchFilterStore.$reset()
            isProcessing = false
        }

        importQuery()
        await exportQuery()
    })
}
