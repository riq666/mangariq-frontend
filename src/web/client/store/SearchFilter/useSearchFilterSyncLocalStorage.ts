import { Pinia } from 'pinia'
import { useRouter } from 'vue-router'
import { useSearchFilterStore } from '.'
import { HydrationKey, loadStateFromLocalStorage, saveStateToLocalStorage } from '../Hydration'

export function useSearchFilterSyncLocalStorage(pinia?: Pinia) {
    if (DEFINE.IS_SSR) {
        return
    }

    const router = useRouter()
    const searchFilterStore = useSearchFilterStore(pinia)

    // Load state from localStorage if there's no init query
    const savedState = loadStateFromLocalStorage(HydrationKey.SEARCH_FILTER)
    const hasNoInitQuery = Object.keys(router.currentRoute.value.query).length === 0
    if (savedState && hasNoInitQuery) {
        searchFilterStore.$patch(savedState)
    }

    // Write changes to localStorage
    searchFilterStore.$subscribe((mutation, state) => {
        saveStateToLocalStorage(HydrationKey.SEARCH_FILTER, state)
    })
}
