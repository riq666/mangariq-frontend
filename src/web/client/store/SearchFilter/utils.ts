import { LocationQuery } from 'vue-router'
import { SearchFilterState } from '.'

export function getQueryString<K extends keyof SearchFilterState>(query: LocationQuery, key: K): string | undefined {
    if (!(key in query)) {
        return
    }

    const val = query[key]
    if (val === null) {
        return
    }

    if (Array.isArray(val)) {
        return
    }

    return val
}

export function getQueryValidatedString<K extends keyof SearchFilterState>(query: LocationQuery, key: K, validValues: Array<string>): SearchFilterState[K] | undefined {
    const s = getQueryString(query, key)
    if (!s) {
        return
    }

    if (!validValues.includes(s)) {
        return
    }

    return s as SearchFilterState[K]
}

export function getQueryArray<K extends keyof SearchFilterState>(query: LocationQuery, key: K): Array<string> {
    if (!(key in query)) {
        return []
    }

    const val = query[key]
    if (val === null) {
        return []
    }

    if (!Array.isArray(val)) {
        return [val]
    }

    return val.filter((val) => Boolean(val)) as Array<string>
}

export function getQueryValidatedArray<K extends keyof SearchFilterState>(query: LocationQuery, key: K, validValues: Array<string>): SearchFilterState[K] {
    const array = getQueryArray(query, key)
    return array.filter((val) => validValues.includes(val)) as SearchFilterState[K]
}
