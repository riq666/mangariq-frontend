import { Pinia } from 'pinia'
import { useUserStore } from '.'
import { HydrationKey, loadStateFromLocalStorage, saveStateToLocalStorage } from '../Hydration'

export function useUserSyncLocalStorage(pinia?: Pinia) {
    if (DEFINE.IS_SSR) {
        return
    }

    const userStore = useUserStore(pinia)

    // Load state from localStorage
    const savedState = loadStateFromLocalStorage(HydrationKey.USER)
    if (savedState) {
        userStore.$patch(savedState)
    }

    // Write changes to localStorage
    userStore.$subscribe((mutation, state) => {
        if (!state.remember) {
            localStorage.removeItem(HydrationKey.USER)
            return
        }

        saveStateToLocalStorage(HydrationKey.USER, state)
    })
}
