import { MD_API_CACHE_KEY } from '@/common/Constants'
import { computed, onMounted, watch } from 'vue'
import { useUserStore } from '.'

export function useCacheInvalidationOnLogout() {
    const userStore = useUserStore()
    const isLoggedIn = computed(() => userStore.isLoggedIn)

    const clearCache = async() => {
        if (isLoggedIn.value) {
            return
        }

        if (!DEFINE.HAS_SERVICE_WORKER) {
            return
        }

        console.info(`Deleting service worker cache (${MD_API_CACHE_KEY})`)
        await caches.delete(MD_API_CACHE_KEY)
    }

    watch(isLoggedIn, clearCache) // Clear api cache if logged out
    onMounted(clearCache) // Always clear service worker cache on app load if user is logged out
}
