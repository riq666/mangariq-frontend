import { ACCESS_TOKEN_LIFESPAN } from '@/common/Constants'
import { fetchLogin, fetchLogout, fetchRefreshToken, LoginPayload } from '@/common/services/MangaDex/auth'
import { sleep } from '@/common/utils/sleep'
import axios from 'axios'
import dayjs from 'dayjs'
import { defineStore } from 'pinia'
import * as Sentry from '@sentry/vue'

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export interface UserState {
    remember: boolean
    tokenExpires: number
    accessToken: string | undefined
    refreshToken: string | undefined
}

export function createDefaultUserState(): UserState {
    const defaultState: UserState = {
        remember: true,
        tokenExpires: NaN,
        accessToken: undefined,
        refreshToken: undefined,
    }

    return defaultState
}

// ----------------------------------------------------------------------------
// Store
// ----------------------------------------------------------------------------

export const useUserStore = defineStore('User', {
    state: createDefaultUserState,

    getters: {
        isLoggedIn: (state) => {
            return Boolean(state.refreshToken)
        },
    },

    actions: {
        toggleRemember() {
            this.remember = !this.remember
        },

        async login(payload: LoginPayload) {
            console.info(`UserAction.LOGIN username:${payload.username}`)

            const data = await fetchLogin(payload)
            this.$patch({
                accessToken: data.token.session,
                refreshToken: data.token.refresh,
                tokenExpires: dayjs().unix() + ACCESS_TOKEN_LIFESPAN,
            })
        },

        async logout() {
            const accessToken = await this.getValidToken()
            if (accessToken) {
                await fetchLogout(accessToken)
            }

            // Always clear data on frontend
            this.$reset()
        },

        async refreshTokens() {
            if (!this.refreshToken) {
                throw new Error('Invalid refreshToken')
            }

            const data = await fetchRefreshToken(this.refreshToken)
            this.$patch({
                accessToken: data.token.session,
                refreshToken: data.token.refresh,
                tokenExpires: dayjs().unix() + ACCESS_TOKEN_LIFESPAN,
            })
        },

        async getValidToken(): Promise<string | undefined> {
            if (!this.isLoggedIn) {
                return undefined
            }

            const now = dayjs()
            const expiration = dayjs.unix(isNaN(this.tokenExpires) ? 0 : this.tokenExpires)
            if (now.isAfter(expiration)) {
                try {
                    await this.refreshTokens()
                } catch (err) {
                    if (axios.isAxiosError(err) && err.response) {
                        const status = err.response.status
                        if (status === 429) {
                            await sleep(1000)
                            return await this.getValidToken()
                        }
                        if (status >= 400 && status < 500) {
                            Sentry.captureException(err)
                            this.$reset()
                            return undefined
                        }
                    }

                    throw err
                }
            }

            return this.accessToken
        },
    },
})
