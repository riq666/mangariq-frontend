import { FollowsFilterState } from './FollowsFilter'
import { SearchFilterState } from './SearchFilter'
import { UserState } from './User'

export enum HydrationKey {
    USER = '__INITIAL_USER_STATE__',
    FOLLOWS_FILTER = '__INITIAL_FOLLOWS_FILTER_STATE__',
    SEARCH_FILTER = '__INITIAL_SEARCH_FILTER_STATE__',
}

interface HydrationStateMap {
    [HydrationKey.USER]: UserState
    [HydrationKey.FOLLOWS_FILTER]: FollowsFilterState
    [HydrationKey.SEARCH_FILTER]: SearchFilterState
}

export function saveStateToLocalStorage<K extends keyof HydrationStateMap>(key: K, state: HydrationStateMap[K]): void {
    if (DEFINE.IS_SSR) {
        return
    }

    const stateString = JSON.stringify(state, replacer)
    localStorage.setItem(key, stateString)
}

export function loadStateFromLocalStorage<K extends keyof HydrationStateMap>(key: K): HydrationStateMap[K] | undefined {
    if (DEFINE.IS_SSR) {
        return
    }

    const state = window[key] ?? localStorage.getItem(key)
    if (!state) {
        return
    }

    return JSON.parse(state, reviver) as HydrationStateMap[K]
}

export function cleanLocalStorage(): void {
    if (DEFINE.IS_SSR) {
        return
    }

    const hydrationKeys = Object.values(HydrationKey)

    for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i)
        if (!key) {
            continue
        }

        if (hydrationKeys.includes(key as HydrationKey)) {
            continue
        }

        console.info(`Removing "${key}" from localStorage`)
        localStorage.removeItem(key)
    }
}

// ----------------------------------------------------------------------------
// Helpers
// ----------------------------------------------------------------------------

function replacer(key: unknown, value: unknown): unknown {
    if (value instanceof Map) {
        return {
            dataType: 'Map',
            value: [...value.entries()],
        }
    } else if (value instanceof Set) {
        return {
            dataType: 'Set',
            value: [...value],
        }
    } else {
        return value
    }
}

function reviver(key: unknown, value: unknown): unknown {
    if (typeof value !== 'object' || value === null) {
        return value
    }

    if (!('dataType' in value && 'value' in value)) {
        return value
    }

    const typedValue = value as { dataType: string; value: unknown }
    switch (typedValue.dataType) {
        case 'Map': {
            return new Map(typedValue.value as Array<[unknown, unknown]>)
        }
        case 'Set': {
            return new Set(typedValue.value as Array<[unknown]>)
        }
    }

    return value
}
