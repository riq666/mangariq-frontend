import { ContentRating, Manga, MangaQuery, MangaStatus, ReadingStatus } from '@/common/services/MangaDex/schemas/Manga'
import { Chapter, ChapterQuery, getChapterTitle } from '@/common/services/MangaDex/schemas/Chapter'
import { ScanlationGroup } from '@/common/services/MangaDex/schemas/ScanlationGroup'
import { Tag } from '@/common/services/MangaDex/schemas/Tag'
import { Artist, Author } from '@/common/services/MangaDex/schemas/Author'
import { defineStore } from 'pinia'
import { AttributeType, findAllRelationships, findRelationship, mapRelationships, PaginatedResult } from '@/common/services/MangaDex/schemas/Result'
import { arrayPush, mapDelete, mapGet, mapGetAll, setAdd } from '@/common/utils/undefinedKeyNoop'
import { RouteName } from '@/web/client/router/routes'
import { getImage } from '@/web/client/utils/ResponsiveLoader'
import { getMangaTitle } from '@/common/services/MangaDex/getters/getMangaTitle'
import { getMangaDesc } from '@/common/services/MangaDex/getters/getMangaDesc'
import MarkdownIt from 'markdown-it'
import { capitalize } from 'lodash'
import { SearchFilterState } from '@/web/client/store/SearchFilter'
import { getSourceName, getSourceUrl, OfficialSite, TrackingSite } from '@/common/services/MangaDex/schemas/ExternalSite'
import { ChapterGroup, groupChaptersByChapter } from '@/common/services/MangaDex/schemas/ChapterGroup'
import { RouteLocationRaw } from 'vue-router'
import { fetchMangas, fetchMangasByIds, fetchSeasonalMangas, purgeCachedManga } from '@/common/services/MangaDex/manga'
import { getCoverUrl } from '@/common/services/MangaDex/schemas/Cover'
import { uniqueArray } from '@/common/utils/uniqueArray'
import { fetchMangasReadChapters, fetchMarkChaptersAsRead } from '@/common/services/MangaDex/mangaRead'
import { fetchIsMangaFollowed, fetchMarkMangaAsFollowed } from '@/common/services/MangaDex/followsIsFollowed'
import { fetchMangaReadingStatus, fetchMangasReadingStatus, fetchSetMangaReadingStatus } from '@/common/services/MangaDex/followsReadingStatus'
import { getPaginationQuery } from '@/common/services/MangaDex/utils/getPaginationQuery'
import { fetchUserFollows, fetchUserFollowsFeed } from '@/common/services/MangaDex/follows'
import { MAX_IDS } from '@/common/Constants'
import { FollowsChapterFeedQuery } from '@/common/services/MangaDex/schemas/Follows'
import { fetchMangaFeed } from '@/common/services/MangaDex/mangaFeed'
import { fetchTags, purgeCachedTags } from '@/common/services/MangaDex/tag'
import { fetchChaptersByIds, fetchChapters } from '@/common/services/MangaDex/chapter'
import { fetchAuthorByName, fetchAuthorsByIds } from '@/common/services/MangaDex/author'

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

type MangaId = string
type ChapterId = string
type ScanlationGroupId = string
type TagId = string
type CreatorId = string

interface MangaTag {
    id: string
    label: string
    route: RouteLocationRaw
}

interface MangaAuthor {
    id: string
    label: string
    title: string
    icon: string
    route: RouteLocationRaw
}

interface MangaExternalSite {
    label: string
    url: string
}

export interface MangaState {
    manga: Map<MangaId, Manga>
    coverArt: Map<MangaId, string>
    isFollowed: Map<MangaId, boolean>
    mangaChapters: Map<MangaId, Set<ChapterId>>
    mangaReadingStatus: Map<MangaId, ReadingStatus | null>

    chapter: Map<ChapterId, Chapter>
    readChapters: Set<ChapterId>

    scanlationGroup: Map<ScanlationGroupId, ScanlationGroup>
    tag: Map<TagId, Tag>
    creator: Map<CreatorId, Author | Artist>
}

function createDefaultMangaState(): MangaState {
    const defaultState: MangaState = {
        manga: new Map(),
        coverArt: new Map(),
        isFollowed: new Map(),
        mangaChapters: new Map(),
        mangaReadingStatus: new Map(),

        chapter: new Map(),
        readChapters: new Set(),

        scanlationGroup: new Map(),
        tag: new Map(),
        creator: new Map(),
    }

    return defaultState
}

// ----------------------------------------------------------------------------
// Store
// ----------------------------------------------------------------------------

export const useMangaStore = defineStore('Manga', {
    state: createDefaultMangaState,

    actions: {
        async deleteManga(mangaId: MangaId) {
            const manga = mapGet(this.manga, mangaId)
            if (manga === undefined) {
                throw new Error('Trying to delete invalid mangaId')
            }

            // Purge cache
            const coverId = findRelationship(manga, AttributeType.CoverArt)?.id
            const scanlationGroupIds = this.getMangaScanlationGroups(mangaId)
            const authorIds = findAllRelationships(manga, AttributeType.Author).map((rel) => rel.id)
            const artistIds = findAllRelationships(manga, AttributeType.Artist).map((rel) => rel.id)
            await purgeCachedManga(mangaId, coverId, scanlationGroupIds, [...authorIds, ...artistIds])

            // Remove manga from store
            for (const chapterId of this.mangaChapters.get(mangaId) ?? []) {
                const chapter = this.chapter.get(chapterId)
                const scanlationGroupId = findRelationship(chapter, AttributeType.ScanlationGroup)?.id
                mapDelete(this.scanlationGroup, scanlationGroupId)

                this.chapter.delete(chapterId)
                this.readChapters.delete(chapterId)
            }

            this.manga.delete(mangaId)
            this.coverArt.delete(mangaId)
            this.isFollowed.delete(mangaId)
            this.mangaChapters.delete(mangaId)
            this.mangaReadingStatus.delete(mangaId)
        },

        async deleteTags() {
            await purgeCachedTags()
            this.tag.clear()
        },

        getMangaTitle(mangaId: MangaId): string {
            const manga = this.manga.get(mangaId)
            return getMangaTitle(manga?.attributes)
        },

        getMangaAltTitles(mangaId: MangaId): Array<string> {
            const titles: Array<string> = []

            for (const title of this.manga.get(mangaId)?.attributes.altTitles ?? []) {
                arrayPush(titles, title.en)
            }

            return titles
        },

        getMangaDesc(mangaId: MangaId): string {
            const manga = this.manga.get(mangaId)
            const desc = getMangaDesc(manga?.attributes)
            const md = new MarkdownIt()
            return md.render(desc)
        },

        getMangaRoute(mangaId: MangaId): RouteLocationRaw {
            return {
                name: RouteName.Manga,
                params: {
                    mangaId,
                },
            }
        },

        getMangaCoverUrl(mangaId: MangaId): string | undefined {
            const mangaIsLoaded = Boolean(this.manga.get(mangaId))
            if (!mangaIsLoaded) {
                return undefined
            }

            return this.coverArt.get(mangaId) ?? getImage('missing-cover.jpg').src
        },

        getMangaOriginalLanguageFlag(mangaId: MangaId): string | undefined {
            const manga = this.manga.get(mangaId)
            const origin = manga?.attributes.originalLanguage
            if (!origin) {
                return undefined
            }

            try {
                const flagUrl = getImage(`flags/${origin}.png`)
                return flagUrl.src
            } catch (err) {
                console.warn(`Cannot find flag image for originalLanguage:${origin}`)
                return undefined
            }
        },

        getMangaContentRatingIcon(mangaId: MangaId): string | undefined {
            const manga = this.manga.get(mangaId)
            switch (manga?.attributes.contentRating) {
                case ContentRating.Safe: {
                    return 'security'
                }
                case ContentRating.Suggestive: {
                    return 'warning'
                }
                case ContentRating.Erotica: {
                    return 'explicit'
                }
                case ContentRating.Pornographic: {
                    return 'favorite'
                }
            }

            return undefined
        },

        getMangaContentRatingColor(mangaId: MangaId): string | undefined {
            const manga = this.manga.get(mangaId)
            switch (manga?.attributes.contentRating) {
                case ContentRating.Safe: {
                    return 'positive'
                }
                case ContentRating.Suggestive: {
                    return 'warning'
                }
                case ContentRating.Erotica: {
                    return 'primary'
                }
                case ContentRating.Pornographic: {
                    return 'negative'
                }
            }

            return undefined
        },

        getMangaStatusIcon(mangaId: MangaId): string | undefined {
            const manga = this.manga.get(mangaId)
            switch (manga?.attributes.status) {
                case MangaStatus.Ongoing: {
                    return 'play_circle_filled'
                }
                case MangaStatus.Completed: {
                    return 'check_circle'
                }
                case MangaStatus.Hiatus: {
                    return 'pending'
                }
                case MangaStatus.Cancelled: {
                    return 'cancel'
                }
            }

            return undefined
        },

        getMangaStatusColor(mangaId: MangaId): string | undefined {
            const manga = this.manga.get(mangaId)
            switch (manga?.attributes.status) {
                case MangaStatus.Ongoing: {
                    return 'positive'
                }
                case MangaStatus.Completed: {
                    return 'info'
                }
                case MangaStatus.Hiatus: {
                    return 'warning'
                }
                case MangaStatus.Cancelled: {
                    return 'negative'
                }
            }

            return undefined
        },

        getMangaScanlationGroups(mangaId: MangaId): Array<ScanlationGroupId> {
            const groupIds = new Set<string>()

            for (const chapterId of this.mangaChapters.get(mangaId) ?? []) {
                const chapter = this.chapter.get(chapterId)
                const groupId = findRelationship(chapter, AttributeType.ScanlationGroup)?.id
                setAdd(groupIds, groupId)
            }

            return [...groupIds]
        },

        getMangaTags(mangaId: MangaId): Map<string, Array<MangaTag>> {
            const manga = this.manga.get(mangaId)
            const tagsGroups = new Map<string, Array<MangaTag>>()

            for (const tag of manga?.attributes.tags ?? []) {
                const enTag = tag.attributes.name.en
                if (!enTag) {
                    continue
                }

                const group = capitalize(tag.attributes.group)
                if (!tagsGroups.has(group)) {
                    tagsGroups.set(group, [])
                }

                const query: Partial<SearchFilterState> = { includedTags: [tag.id] }
                tagsGroups.get(group)?.push({
                    id: tag.id,
                    label: enTag,
                    route: {
                        name: RouteName.Search,
                        query: {
                            ...query,
                            page: 1,
                        },
                    },
                })
            }

            return tagsGroups
        },

        getMangaCreators(mangaId: MangaId): Array<MangaAuthor> {
            const manga = this.manga.get(mangaId)
            const creators: Array<MangaAuthor> = []

            const addCreator = (creatorId: CreatorId, prefix: string, icon: string, queryKey: keyof SearchFilterState) => {
                const creatorName = this.creator.get(creatorId)?.attributes.name
                if (!creatorName) {
                    return
                }

                const query: Partial<SearchFilterState> = { [queryKey]: [creatorId] }
                creators.push({
                    id: creatorId,
                    title: `${prefix}: ${creatorName}`,
                    label: creatorName,
                    icon,
                    route: {
                        name: RouteName.Search,
                        query: {
                            ...query,
                            page: 1,
                        },
                    },
                })
            }

            findAllRelationships(manga, AttributeType.Author).forEach((author) => addCreator(author.id, 'Author', 'face', 'authors'))
            findAllRelationships(manga, AttributeType.Artist).forEach((artist) => addCreator(artist.id, 'Artist', 'brush', 'artists'))

            return creators
        },

        getMangaOfficialSites(mangaId: MangaId): Array<MangaExternalSite> {
            const manga = this.manga.get(mangaId)
            const links: Array<MangaExternalSite> = []

            for (const src of Object.values(OfficialSite)) {
                const val = manga?.attributes.links?.[src]
                if (!val) {
                    continue
                }

                links.push({
                    label: getSourceName(src),
                    url: getSourceUrl(src, val),
                })
            }

            return links
        },

        getMangaTrackingSites(mangaId: MangaId): Array<MangaExternalSite> {
            const manga = this.manga.get(mangaId)
            const links: Array<MangaExternalSite> = []

            for (const src of Object.values(TrackingSite)) {
                const val = manga?.attributes.links?.[src]
                if (!val) {
                    continue
                }

                links.push({
                    label: getSourceName(src),
                    url: getSourceUrl(src, val),
                })
            }

            return links
        },

        getMangaChapterGroups(mangaId: MangaId): Array<ChapterGroup> {
            const chapters: Array<Chapter> = []

            for (const chapterId of this.mangaChapters.get(mangaId) ?? []) {
                const chapter = this.chapter.get(chapterId)
                arrayPush(chapters, chapter)
            }

            return groupChaptersByChapter(chapters)
        },

        getMangaNextChapterToRead(mangaId: MangaId): ChapterId | undefined {
            const chapterGroups = this.getMangaChapterGroups(mangaId)

            for (let i = chapterGroups.length - 1; i >= 0; i--) {
                const chapterGroup = chapterGroups[i]
                const hasReadChapter = Boolean(chapterGroup.chapterIds.find((chapterId) => this.readChapters?.has(chapterId)))
                if (hasReadChapter) {
                    continue
                }

                return chapterGroup.chapterIds[0]
            }

            return undefined
        },

        getChapterTitle(chapterId?: ChapterId): string {
            const chapter = mapGet(this.chapter, chapterId)
            return getChapterTitle(chapter)
        },

        getChapterRoute(chapterId: ChapterId): RouteLocationRaw {
            return {
                name: RouteName.Reader,
                params: {
                    chapterId,
                    page: 1,
                },
            }
        },

        getChapterScanlationGroup(chapterId: ChapterId): ScanlationGroup | undefined {
            const chapter = mapGet(this.chapter, chapterId)
            const groupId = findRelationship(chapter, AttributeType.ScanlationGroup)?.id
            return mapGet(this.scanlationGroup, groupId)
        },

        // --------------------------------------------------------------------
        // Processing Helpers
        // --------------------------------------------------------------------

        async processFetchedMangas(mangas: Array<Manga>, fetchRelatedMangas = false) {
            for (const manga of mangas) {
                // Save mangas
                const mangaId = manga.id
                this.manga.set(mangaId, manga)

                // Save coverArt
                const coverAttrs = findRelationship(manga, AttributeType.CoverArt)?.attributes
                const coverUrl = getCoverUrl(mangaId, coverAttrs)
                if (coverUrl) {
                    this.coverArt.set(mangaId, coverUrl)
                }

                // Save creators
                const creators = [
                    ...findAllRelationships(manga, AttributeType.Author),
                    ...findAllRelationships(manga, AttributeType.Artist),
                ]
                for (const creator of creators) {
                    if (!creator.attributes) {
                        continue
                    }

                    this.creator.set(creator.id, {
                        id: creator.id,
                        type: creator.type,
                        attributes: creator.attributes,
                    })
                }
            }

            if (fetchRelatedMangas) {
                const unfetchedRelatedMangaIds = new Array<string>()

                for (const manga of mangas) {
                    const relatedMangas = findAllRelationships(manga, AttributeType.Manga)
                    for (const relatedManga of relatedMangas) {
                        if (!relatedManga.attributes) {
                            continue
                        }

                        if (relatedManga.id === manga.id) {
                            continue
                        }

                        if (this.manga.has(relatedManga.id)) {
                            continue
                        }

                        unfetchedRelatedMangaIds.push(relatedManga.id)
                    }
                }

                const unfetchedRelatedMangas = await fetchMangasByIds(unfetchedRelatedMangaIds, { contentRating: Object.values(ContentRating) })
                await this.processFetchedMangas(unfetchedRelatedMangas)
            }
        },

        async processFetchedChapters(chapters: Array<Chapter>, accessToken?: string, commitAllMangaAsFollowed = false) {
            if (chapters.length === 0) {
                return
            }

            for (const chapter of chapters) {
                this.chapter.set(chapter.id, chapter)

                const mangaId = findRelationship(chapter, AttributeType.Manga)?.id
                if (mangaId) {
                    if (!this.mangaChapters.has(mangaId)) {
                        this.mangaChapters.set(mangaId, new Set())
                    }

                    this.mangaChapters.get(mangaId)?.add(chapter.id)
                }

                const scanlationGroups = findAllRelationships(chapter, AttributeType.ScanlationGroup)
                for (const group of scanlationGroups) {
                    if (!group.attributes) {
                        continue
                    }

                    this.scanlationGroup.set(group.id, {
                        ...group,
                        attributes: group.attributes,
                    })
                }
            }

            // Fetch corresponding manga info for the new chapters
            const mangaIds = uniqueArray(mapRelationships(chapters, AttributeType.Manga).map((rel) => rel.id))

            await Promise.all([
                (async() => {
                    const missingMangaIds = mangaIds.filter((mangaId) => !this.manga.has(mangaId))
                    const mangas = await fetchMangasByIds(missingMangaIds, { contentRating: Object.values(ContentRating) })
                    await this.processFetchedMangas(mangas)
                })(),
                (() => {
                    if (!commitAllMangaAsFollowed) {
                        return
                    }

                    mangaIds.forEach((mangaId) => this.isFollowed.set(mangaId, true))
                })(),
                (async() => {
                    if (!accessToken) {
                        return
                    }

                    const readChapterIds = await fetchMangasReadChapters(accessToken, mangaIds)
                    readChapterIds.forEach((chapterId) => this.readChapters.add(chapterId))
                })(),
            ])
        },

        // --------------------------------------------------------------------
        // API Requests
        // --------------------------------------------------------------------

        async fetchManga(mangaId: MangaId, accessToken?: string): Promise<Manga> {
            const manga = (await fetchMangasByIds([mangaId]))[0]
            await this.processFetchedMangas([manga], true)

            // When payload.id is 'random', we need to update payload with the actual id so it can be reused below
            mangaId = manga.id

            // Fetch other manga info after manga has been commited to Map
            await Promise.all([
                (async() => {
                    if (!accessToken) {
                        return
                    }

                    const isFollowed = await fetchIsMangaFollowed(accessToken, mangaId)
                    this.isFollowed.set(mangaId, isFollowed)
                })(),
                (async() => {
                    if (!accessToken) {
                        return
                    }

                    const readingStatus = await fetchMangaReadingStatus(accessToken, mangaId)
                    this.mangaReadingStatus.set(mangaId, readingStatus)
                })(),
                (async() => {
                    await this.fetchMangaChapters(mangaId, accessToken)
                })(),
            ])

            return manga
        },

        async fetchMangas(query?: MangaQuery): Promise<PaginatedResult<AttributeType.Manga>> {
            const response = await fetchMangas(query)
            await this.processFetchedMangas(response.data)
            return response
        },

        async fetchSeasonalMangas(): Promise<Array<Manga>> {
            const seasonalMangas = await fetchSeasonalMangas()
            await this.processFetchedMangas(seasonalMangas)
            return seasonalMangas
        },

        async fetchUserFollowedMangas(accessToken?: string): Promise<Array<Manga>> {
            if (!accessToken) {
                return []
            }

            let followedMangas: Array<Manga> = []
            let hasMorePages = true
            let currentPage = 0

            while (hasMorePages) {
                const query = getPaginationQuery(currentPage, MAX_IDS)
                const response = await fetchUserFollows(accessToken, query)
                followedMangas = followedMangas.concat(response.data)
                hasMorePages = response.offset + response.limit < response.total
                currentPage += 1
            }

            // Fetch other manga info after manga has been commited to Map
            await Promise.all([
                (async() => {
                    await this.processFetchedMangas(followedMangas)
                })(),
                (() => {
                    for (const manga of followedMangas) {
                        const mangaId = manga.id
                        this.isFollowed.set(mangaId, true)
                    }
                })(),
                (async() => {
                    const readingStatuses = await fetchMangasReadingStatus(accessToken)
                    for (const [mangaId, readingStatus] of Object.entries(readingStatuses)) {
                        this.mangaReadingStatus.set(mangaId, readingStatus)
                    }
                })(),
            ])

            return followedMangas
        },

        async fetchUserFollowedMangasFeed(accessToken?: string, query?: FollowsChapterFeedQuery): Promise<PaginatedResult<AttributeType.Chapter>> {
            if (!accessToken) {
                throw new Error('Missing accessToken')
            }

            const response = await fetchUserFollowsFeed(accessToken, query)
            await this.processFetchedChapters(response.data, accessToken, true)
            return response
        },

        async toggleMangaIsFollowed(mangaId: MangaId, accessToken?: string): Promise<void> {
            if (!accessToken) {
                throw new Error('Missing accessToken')
            }

            const isFollowed = !this.isFollowed.get(mangaId)

            await Promise.all([
                (async() => {
                    await fetchMarkMangaAsFollowed(accessToken, mangaId, isFollowed)
                    this.isFollowed.set(mangaId, isFollowed)
                })(),
                (async() => {
                    if (isFollowed) {
                        await this.setMangaReadingStatus(mangaId, ReadingStatus.Reading, accessToken)
                    } else {
                        await this.setMangaReadingStatus(mangaId, null, accessToken)
                    }
                })(),
            ])
        },

        async setMangaReadingStatus(mangaId: MangaId, readingStatus: ReadingStatus | null, accessToken?: string): Promise<void> {
            if (!accessToken) {
                return
            }

            await fetchSetMangaReadingStatus(accessToken, mangaId, readingStatus)
            this.mangaReadingStatus.set(mangaId, readingStatus)
        },

        async fetchMangaChapters(mangaId: MangaId, accessToken?: string): Promise<void> {
            const chapters = await fetchMangaFeed(mangaId)
            await this.processFetchedChapters(chapters, accessToken)
        },

        async markChapterGroupAsRead(mangaId: MangaId, currentGroup: ChapterGroup, markAsRead: boolean, accessToken?: string): Promise<void> {
            if (!accessToken) {
                return
            }

            const readChapterIds = currentGroup
                .chapterIds // Chapters of same group (i.e. same chapter from multiple groups)
                .filter((chapterId) => markAsRead !== this.readChapters.has(chapterId)) // Filter out chapters that already have the expected read markers

            await fetchMarkChaptersAsRead(accessToken, mangaId, readChapterIds, markAsRead)

            if (markAsRead) {
                readChapterIds.forEach((chapterId) => this.readChapters.add(chapterId))
            } else {
                readChapterIds.forEach((chapterId) => this.readChapters.delete(chapterId))
            }
        },

        async markChaptersAroundChapterGroupAsRead(mangaId: MangaId, currentGroup: ChapterGroup, markAsRead: boolean, markLaterChapters: boolean, accessToken?: string): Promise<void> {
            if (!accessToken) {
                return
            }

            const chapterGroups = this.getMangaChapterGroups(mangaId)
            const currentKey = parseFloat(currentGroup.key)

            let readChapterIds: Array<string> = []
            for (const chapterGroup of chapterGroups) {
                const key = parseFloat(chapterGroup.key)
                if ((markLaterChapters && key <= currentKey) || // If marking later chapters, then skip smaller keys
                    (!markLaterChapters && key >= currentKey)) { // Otherwise marking older chapters, skip greater keys
                    continue
                }

                const chapterIds = chapterGroup
                    .chapterIds // Chapters of same group (i.e. same chapter from multiple groups)
                    .filter((chapterId) => markAsRead !== this.readChapters.has(chapterId)) // Filter out chapters that already have the expected read markers

                readChapterIds = readChapterIds.concat(chapterIds)
            }

            await fetchMarkChaptersAsRead(accessToken, mangaId, readChapterIds, markAsRead)

            if (markAsRead) {
                readChapterIds.forEach((chapterId) => this.readChapters.add(chapterId))
            } else {
                readChapterIds.forEach((chapterId) => this.readChapters.delete(chapterId))
            }
        },

        async fetchChapter(chapterId: ChapterId, accessToken?: string): Promise<Chapter> {
            let chapter = this.chapter.get(chapterId)
            if (!chapter) {
                chapter = (await fetchChaptersByIds([chapterId]))[0]
            }

            // Get other chapters of this chapter's parent manga so we can freely navigate between chapters
            const mangaId = findRelationship(chapter, AttributeType.Manga)?.id
            if (!mangaId) {
                throw new Error(`Cannot find mangaId of chapter:${chapterId}`)
            }

            await this.fetchMangaChapters(mangaId, accessToken)

            return chapter
        },

        async fetchChapters(accessToken?: string, query?: ChapterQuery): Promise<PaginatedResult<AttributeType.Chapter>> {
            const response = await fetchChapters(query)
            await this.processFetchedChapters(response.data, accessToken)
            return response
        },

        async fetchTags(): Promise<void> {
            if (this.tag.size > 0) {
                return
            }

            const tags = await fetchTags()
            tags.forEach((tag) => this.tag.set(tag.id, tag))
        },

        async searchCreatorByName(name: string): Promise<Array<Author | Artist>> {
            const creators = await fetchAuthorByName(name)
            creators.forEach((creator) => this.creator.set(creator.id, creator))
            return creators
        },

        async fetchCreators(creatorIds: Array<CreatorId>): Promise<Array<Author | Artist>> {
            const missingIds = creatorIds.filter((creatorId) => !this.creator.has(creatorId))
            const creators = await fetchAuthorsByIds(missingIds)
            creators.forEach((creator) => this.creator.set(creator.id, creator))
            return mapGetAll(this.creator, creatorIds)
        },
    },
})
