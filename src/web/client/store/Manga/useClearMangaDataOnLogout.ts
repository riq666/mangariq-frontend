import { Pinia } from 'pinia'
import { computed, watch } from 'vue'
import { useMangaStore } from '.'
import { useUserStore } from '../User'

export function useClearMangaDataOnLogout(pinia?: Pinia) {
    const userStore = useUserStore(pinia)
    const mangaStore = useMangaStore(pinia)

    const isLoggedIn = computed(() => userStore.isLoggedIn)
    watch(isLoggedIn, () => {
        if (isLoggedIn.value) {
            return
        }

        mangaStore.$reset()
    })
}
