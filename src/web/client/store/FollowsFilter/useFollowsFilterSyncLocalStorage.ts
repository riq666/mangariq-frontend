import { Pinia } from 'pinia'
import { useFollowsFilterStore } from '.'
import { HydrationKey, loadStateFromLocalStorage, saveStateToLocalStorage } from '../Hydration'

export function useFollowsFilterSyncLocalStorage(pinia?: Pinia) {
    if (DEFINE.IS_SSR) {
        return
    }

    const followsFilterStore = useFollowsFilterStore(pinia)

    // Load state from localStorage
    const savedState = loadStateFromLocalStorage(HydrationKey.FOLLOWS_FILTER)
    if (savedState) {
        followsFilterStore.$patch(savedState)
    }

    // Write changes to localStorage
    followsFilterStore.$subscribe((mutation, state) => {
        saveStateToLocalStorage(HydrationKey.FOLLOWS_FILTER, state)
    })
}
