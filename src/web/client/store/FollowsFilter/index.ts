import { MangaStatus, ReadingStatus } from '@/common/services/MangaDex/schemas/Manga'
import { defineStore } from 'pinia'

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export interface FollowsFilterState {
    mangaStatus: MangaStatus | null
    readingStatus: ReadingStatus | null
    title: string | null
}

function createDefaultFollowsFilterState(): FollowsFilterState {
    const defaultState: FollowsFilterState = {
        mangaStatus: null,
        readingStatus: null,
        title: null,
    }

    return defaultState
}

// ----------------------------------------------------------------------------
// Store
// ----------------------------------------------------------------------------

export const useFollowsFilterStore = defineStore('FollowsFilter', {
    state: createDefaultFollowsFilterState,
})
