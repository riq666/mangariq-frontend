import '@/common/utils/setupDayjs'
import { createApp as createClientApp } from 'vue'
import AppLoader from './client/components/AppLoader.vue'
import ExternalLink from './client/components/Global/ExternalLink.vue'
import SimpleImage from './client/components/Global/SimpleImage.vue'
import { createAppRouter } from './client/router'
import { Router } from 'vue-router'
import { Quasar, Notify, Loading, Dialog } from 'quasar'
import { createMetaManager, defaultConfig } from 'vue-meta'
import { AppContext } from './AppContext'
import { createPinia } from 'pinia'
import { useUserSyncLocalStorage } from './client/store/User/useUserSyncLocalStorage'

interface CreatedApp {
    app: ReturnType<typeof createClientApp>
    router: Router
}

export async function createApp(ssrContext?: AppContext): Promise<CreatedApp> {
    console.info('Release', DEFINE.GIT_HASH)

    // Vue
    const app = createClientApp(AppLoader)
    app.component('ExternalLink', ExternalLink)
    app.component('SimpleImage', SimpleImage)

    // Pinia
    const pinia = createPinia()
    app.use(pinia)

    // Must hydrate from localStorage before router setup so that nav guard gets up-to-date user info
    useUserSyncLocalStorage(pinia)

    // Vue Router
    const router = await createAppRouter(pinia, ssrContext)
    app.use(router)
    await router.isReady()

    // Vue Meta
    const metaManager = createMetaManager(DEFINE.IS_SSR, {
        ...defaultConfig,
        'theme-color': {
            tag: 'meta',
            keyAttribute: 'name',
        },
    })
    app.use(metaManager)

    // Quasar
    app.use(Quasar, {
        plugins: {
            Dialog,
            Notify,
            Loading,
        },
    }, ssrContext)

    return {
        app,
        router,
    }
}
