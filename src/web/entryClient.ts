import './client/assets/css/main.scss'
import { createApp } from './app'
import * as Sentry from '@sentry/vue'
import { Integrations } from '@sentry/tracing'
import { SENTRY_DSN } from '@/common/Constants'
import { cleanLocalStorage } from './client/store/Hydration'

async function main() {
    const { app, router } = await createApp()

    Sentry.init({
        app,
        release: DEFINE.GIT_HASH,
        dsn: SENTRY_DSN,
        integrations: [
            new Integrations.BrowserTracing({
                routingInstrumentation: Sentry.vueRouterInstrumentation(router),
            }),
        ],
        tracesSampleRate: 0,
        enabled: !DEFINE.IS_DEV,
    })

    cleanLocalStorage()
    app.mount('#app')
}

main().catch((err) => {
    console.warn(err)

    if (DEFINE.IS_DEV) {
        const error = err as Error
        console.warn(error.stack)
    }
})
