// ----------------------------------------------------------------------------
// Enable service worker definitions
/// <reference lib="WebWorker" />
declare const self: ServiceWorkerGlobalScope
export {} // Treat this file as a module so the above declarations do not get merged into global namespace
// ----------------------------------------------------------------------------

import { MD_API_URL, FRONTEND_URL, CORS_PROXY_TARGET_ORIGIN_HEADER, CORS_PROXY_URL } from '@/common/Constants'

const ALLOWED_ORIGIN = FRONTEND_URL
const ALLOWED_METHODS = 'GET, POST, DELETE, PUT, OPTIONS'

self.addEventListener('fetch', (event) => {
    switch (event.request.method) {
        case 'OPTIONS': {
            event.respondWith(handleOptions(event.request))
            break
        }
        case 'GET':
        case 'POST':
        case 'DELETE':
        case 'PUT': {
            event.respondWith(handleRequest(event.request))
            break
        }
        default: {
            const res = new Response(null, {
                status: 405,
                statusText: 'Method Not Allowed',
            })

            event.respondWith(res)
        }
    }
})

async function handleRequest(request: Request) {
    const targetOrigin = request.headers.get(CORS_PROXY_TARGET_ORIGIN_HEADER) ?? MD_API_URL
    const proxyUrl = request.url.replace(CORS_PROXY_URL, targetOrigin)
    const proxyReq = new Request(proxyUrl, {
        body: request.body,
        headers: request.headers,
        method: request.method,
        redirect: request.redirect,
    })

    // Delete internally used headers if they exist
    proxyReq.headers.delete(CORS_PROXY_TARGET_ORIGIN_HEADER)

    const proxyRes = await fetch(proxyReq)
    const res = new Response(proxyRes.body, proxyRes)
    res.headers.set('Access-Control-Allow-Origin', ALLOWED_ORIGIN)
    res.headers.append('Vary', 'Origin')
    return res
}

function handleOptions(request: Request) {
    const defaultOptionsRes = new Response(null, {
        headers: {
            Allow: ALLOWED_METHODS,
        },
    })

    const acrOrigin = request.headers.get('Origin')
    if (acrOrigin === null) {
        return defaultOptionsRes
    }

    const acrMethod = request.headers.get('Access-Control-Request-Method')
    if (acrMethod === null) {
        return defaultOptionsRes
    }

    const acrHeaders = request.headers.get('Access-Control-Request-Headers')
    if (acrHeaders === null) {
        return defaultOptionsRes
    }

    return new Response(null, {
        headers: {
            'Access-Control-Allow-Origin': ALLOWED_ORIGIN,
            'Access-Control-Allow-Methods': ALLOWED_METHODS,
            'Access-Control-Max-Age': (24 * 3600).toString(),

            // Allow all future content Request headers to go back to browser
            // such as Authorization (Bearer) or X-Client-Name-Version
            'Access-Control-Allow-Headers': acrHeaders,
        },
    })
}
